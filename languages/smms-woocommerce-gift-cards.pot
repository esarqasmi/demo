#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: YITH WooCommerce Gift Cards\n"
"POT-Creation-Date: 2017-02-07 15:38+0100\n"
"PO-Revision-Date: 2015-04-22 12:59+0100\n"
"Last-Translator: \n"
"Language-Team: Your Inspiration Themes <plugins@yithemes.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"
"X-Poedit-KeywordsList: __ ;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;"
"esc_attr_e;esc_attr_e\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: plugin-fw\n"

#: functions.php:115
msgid "The gift card has been disabled"
msgstr ""

#: functions.php:118
msgid "Valid"
msgstr ""

#: functions.php:121
msgid "No longer valid, replaced by another code"
msgstr ""

#: init.php:41
msgid ""
"YITH WooCommerce Gift Cards is enabled but not effective. It requires "
"WooCommerce in order to work."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:270
#: lib/class-yith-woocommerce-gift-cards-premium.php:275
#: lib/class-yith-ywgc-backend.php:502 lib/class-yith-ywgc-backend.php:536
msgid "Disabled"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:275
#: lib/class-yith-woocommerce-gift-cards-premium.php:285
#, php-format
msgid "<span class=\"count\"> (%s)</span>"
msgid_plural " <span class=\"count\"> (%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:280
#: lib/class-yith-woocommerce-gift-cards-premium.php:285
msgid "Dismissed"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:393
#: lib/class-yith-ywgc-backend.php:311
msgid "Gift card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:394
msgid "gift_card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:395
msgid ""
"This product has been automatically created by the plugin YITH Gift Cards."
"You must not edit it, or the plugin could not work properly"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:423
msgid "An error occurred, you cannot use the plugin"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:542
msgid "Gift card detail"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:548
#: lib/class-ywgc-plugin-fw-loader.php:128
msgid "General"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:552
msgid "Purchased amount"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:553
msgid "The amount purchased by the customer."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:559
msgid "Purchased amount tax"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:560
msgid "The tax amount purchased by the customer."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:566
msgid "Current balance"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:567
msgid "The current amount available for the customer."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:573
msgid "Tax current balance"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:574
msgid "The current tax amount left available for the gift card receiver."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:584
msgid "Digital"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:585
msgid ""
"Choose whether the gift card will be sent via email or like a physical "
"product."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:591
msgid "Sender name"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:592
msgid "The sender name, if any, of the digital gift card."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:603
#: lib/class-yith-ywgc-cart-checkout.php:132
#: lib/class-yith-ywgc-cart-checkout.php:133
msgid "Recipient email"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:604
msgid "The recipient email address of the digital gift card."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:614
#: templates/yith-gift-cards/gift-card-details.php:53
msgid "Message"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:615
msgid "The message attached to the gift card."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:625
msgid "Delivery date"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:626
msgid "The date when the digital gift card will be sent to the recipient."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:651
msgctxt "post type general name"
msgid "Gift Cards"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:652
msgctxt "post type singular name"
msgid "Gift Card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:653
msgctxt "admin menu"
msgid "Gift Cards"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:654
msgctxt "add new on admin bar"
msgid "Gift Card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:655
msgctxt "admin menu item"
msgid "Add New"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:656
msgid "Add New Gift Card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:657
msgid "New Gift Card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:658
msgid "Edit Gift Card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:659
msgid "View Gift Card"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:660
msgid "All gift cards"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:661
msgid "Search gift cards"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:662
msgid "Parent gift cards:"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:663
msgid "No gift cards found."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:664
msgid "No gift cards found in Trash."
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:666
#: lib/class-yith-woocommerce-gift-cards-premium.php:667
#: lib/class-yith-woocommerce-gift-cards.php:225
#: lib/class-yith-woocommerce-gift-cards.php:226
msgid "Gift Cards"
msgstr ""

#: lib/class-yith-woocommerce-gift-cards-premium.php:836
#: templates/yith-gift-cards/gift-card-details.php:55
msgid "Your message..."
msgstr ""

#: lib/class-yith-ywgc-backend-premium.php:422
#: lib/class-yith-ywgc-backend-premium.php:423
msgid "Send now"
msgstr ""

#: lib/class-yith-ywgc-backend-premium.php:442
msgid "Enable"
msgstr ""

#: lib/class-yith-ywgc-backend-premium.php:446
msgid "Disable"
msgstr ""

#: lib/class-yith-ywgc-backend-premium.php:502
msgid "Gift card code: "
msgstr ""

#: lib/class-yith-ywgc-backend-premium.php:510
msgid "Enter the pre-printed code: "
msgstr ""

#: lib/class-yith-ywgc-backend.php:251
msgid "Choose Image"
msgstr ""

#: lib/class-yith-ywgc-backend.php:275
msgid "Set gift card category"
msgstr ""

#: lib/class-yith-ywgc-backend.php:276
msgid "Unset gift card category"
msgstr ""

#: lib/class-yith-ywgc-backend.php:492
msgid "Variable amount mode"
msgstr ""

#: lib/class-yith-ywgc-backend.php:496 lib/class-yith-ywgc-backend.php:530
msgid "Default"
msgstr ""

#: lib/class-yith-ywgc-backend.php:499 lib/class-yith-ywgc-backend.php:533
msgid "Enabled"
msgstr ""

#: lib/class-yith-ywgc-backend.php:526
msgid "Show template design"
msgstr ""

#: lib/class-yith-ywgc-backend.php:554
#: templates/yith-gift-cards/gift-card-template.php:31
#: templates/yith-gift-cards/gift-card-template.php:32
msgid "Gift card image"
msgstr ""

#: lib/class-yith-ywgc-backend.php:561
msgid "No image selected, the featured image will be used"
msgstr ""

#: lib/class-yith-ywgc-backend.php:566
msgid "Choose image"
msgstr ""

#: lib/class-yith-ywgc-backend.php:571
msgid "Reset image"
msgstr ""

#: lib/class-yith-ywgc-backend.php:598
msgid "Gift card amount"
msgstr ""

#: lib/class-yith-ywgc-backend.php:602
msgid "Add"
msgstr ""

#: lib/class-yith-ywgc-backend.php:638
msgid "You don't have configured any gift card yet"
msgstr ""

#: lib/class-yith-ywgc-cart-checkout.php:118
#: lib/class-yith-ywgc-cart-checkout.php:119
#: templates/yith-gift-cards/gift-card-details.php:43
msgid "Recipient name"
msgstr ""

#: lib/class-yith-ywgc-cart-checkout.php:128
msgid "Your billing email"
msgstr ""

#: lib/class-yith-ywgc-cart-checkout.php:170
msgid "The gift card has invalid amount"
msgstr ""

#: lib/class-yith-ywgc-cart-checkout.php:306
msgid "An error occurred while adding the product to the cart."
msgstr ""

#: lib/class-yith-ywgc-cart-checkout.php:321
msgid "Add a valid email for the recipient"
msgstr ""

#: lib/class-yith-ywgc-cart-checkout.php:334
msgid "The recipient(s) email address is mandatory"
msgstr ""

#: lib/class-yith-ywgc-cart-checkout.php:345
msgid "Email address not valid, please check the following: "
msgstr ""

#: lib/class-yith-ywgc-emails.php:247
#, php-format
msgctxt "gift card expiration date"
msgid "This gift card code will be valid until %s (Y-m-d)"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:193
#: lib/class-yith-ywgc-frontend-premium.php:433
msgid "Cancel"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:260
msgid "The gift card code is already applied to the current cart."
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:264
msgid "Gift card code applied successfully."
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:268
msgid "Gift card code removed successfully."
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:273
#, php-format
msgid "The gift card code %s is expired and cannot be used anymore."
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:275
#, php-format
msgid "The gift card code %s is disabled, please contact the vendor."
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:277
#, php-format
msgid "The gift card code %s does not exist!"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:379
msgid "See card details"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:382
#: lib/class-yith-ywgc-frontend-premium.php:403
#: templates/yith-gift-cards/gift-card-details.php:14
msgid "Gift card details"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:384
#: lib/class-yith-ywgc-frontend-premium.php:410
msgid "Sender: "
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:389
#: lib/class-yith-ywgc-frontend-premium.php:417
msgid "Recipient: "
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:394
#: lib/class-yith-ywgc-frontend-premium.php:424
msgid "Message: "
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:399
msgid "Edit"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:431
msgid "Apply"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:529
msgid "Enter amount(Only digits)"
msgstr ""

#: lib/class-yith-ywgc-frontend-premium.php:560
msgid "Manual amount"
msgstr ""

#: lib/class-yith-ywgc-frontend.php:195
msgid "File format is not valid, select a jpg, jpeg, png, gif or bmp file"
msgstr ""

#: lib/class-yith-ywgc-frontend.php:196
msgid ""
"The size fo the uploaded file exceeds the maximum allowed ({YITH_YWGC()-"
">custom_image_max_size} MB)"
msgstr ""

#: lib/class-yith-ywgc-frontend.php:198
msgid ""
"<b>Attention</b>: the <b>suggested minimum</b> size of the image is 490x195"
msgstr ""

#: lib/class-yith-ywgc-frontend.php:199
msgid ""
"You have selected more than one recipient: a gift card for each recepient "
"will be generated."
msgstr ""

#: lib/class-yith-ywgc-frontend.php:200
msgid "Please enter a valid delivery date"
msgstr ""

#: lib/class-yith-ywgc-frontend.php:213
#, php-format
msgctxt ""
"Alert: the manual gift card field was filled with a wrong formatted value. "
"It should contains only digits and a facultative decimal separator followed "
"by one or two digits"
msgid ""
"Please use only digits and the decimal separator '%1$s'. Valid examples are "
"'123', '123%1$s9 and '123%1$s99'."
msgstr ""

#: lib/class-yith-ywgc-frontend.php:216
msgid "Please enter a valid email address"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:67
msgid "Order"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:68
#: templates/single-product/add-to-cart/gift-card.php:34
msgid "Amount"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:69
#: templates/myaccount/my-giftcards.php:16
msgid "Balance"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:70
msgid "Orders"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:71
msgid "Order total"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:72
msgid "Information"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:114
msgid "Guest"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:118
#, php-format
msgctxt "Order number by X"
msgid "%s by %s"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:145
msgid "Created manually"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:176
#: templates/myaccount/my-giftcards.php:96
msgid "The code has not been used yet"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:207
#, php-format
msgid "(+ %.2f%%)"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:232
msgid "This card is dismissed."
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:239
msgid "Physical product"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:246
#, php-format
msgid "Sent on %s"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:249
msgid "Scheduled"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:252
msgid "Failed"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:257
#, php-format
msgid "Recipient: %s"
msgstr ""

#: lib/class-yith-ywgc-gift-cards-table.php:260
#, php-format
msgid "Delivery date: %s"
msgstr ""

#: lib/class-yith-ywgc-product.php:157
#: lib/third-party/class-ywgc-dynamic-pricing.php:90
#, php-format
msgctxt "Price range: from-to"
msgid "%1$s&ndash;%2$s"
msgstr ""

#: lib/class-yith-ywgc-product.php:204
msgid "Select amount"
msgstr ""

#: lib/class-yith-ywgc-shipping.php:160
#, php-format
msgctxt "In Cart/Checkout page: %s stands for the gift card amount."
msgid "(%s shipping cost discount included for the selected shipping method)"
msgstr ""

#: lib/class-yith-ywgc-shipping.php:304
msgid "Gift card:"
msgstr ""

#: lib/class-yith-ywgc-shipping.php:393
#, php-format
msgctxt ""
"In cart/checkout: %s is the original shipping price, before the discount"
msgid "(In place of %s, thanks to gift card discount)"
msgstr ""

#: lib/class-yith-ywgc-shipping.php:467
#, php-format
msgctxt "In cart/checkout: about a shipping method discounted with a gift card"
msgid "In place of %s, thanks to gift card discount"
msgstr ""

#: lib/class-yith-ywgc-shipping.php:479
#, php-format
msgid "via %s"
msgstr ""

#: lib/class-yith-ywgc-shipping.php:491
#, php-format
msgctxt ""
"In Cart/Checkout page state that a gift card is used for discounting "
"shipping cost"
msgid "(Includes %s discount on shipping fees because of the gift card used)"
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:129
msgid "Template"
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:132
#: lib/class-ywgc-plugin-fw-loader.php:195
msgid "Premium Version"
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:191
msgid "Settings"
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:221
msgid "Plugin Documentation"
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:234
msgid ""
"YITH WooCommerce Gift Cards is available in an outstanding PREMIUM version "
"with many new options, discover it now."
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:235
msgid "Premium version"
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:242
msgid "YITH WooCommerce Gift Cards"
msgstr ""

#: lib/class-ywgc-plugin-fw-loader.php:243
msgid ""
"In the YITH Plugins tab you can find YITH WooCommerce Gift Cards options."
"<br> From this menu you can access all the settings of your active YITH "
"plugins."
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:33
msgid "YITH Gift Cards - Notification"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:36
msgid "Customer notification - the coupon you have bought has been used"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:39
msgid "I have appreciated your gift"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:40
msgid "[{site_title}] Your gift card has been used"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:46
#: lib/emails/class-yith-ywgc-email-notify-customer.php:81
msgid ""
"The gift card you have sent to <code>{recipient_email}</code> has been used "
"in our shop."
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:135
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:141
msgid "Enable/Disable"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:137
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:143
msgid "Enable this email notification"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:141
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:147
msgid "Subject"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:143
#: lib/emails/class-yith-ywgc-email-notify-customer.php:150
#: lib/emails/class-yith-ywgc-email-notify-customer.php:157
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:149
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:156
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:163
#, php-format
msgid "Defaults to <code>%s</code>"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:148
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:154
msgid "Email Heading"
msgstr ""

#: lib/emails/class-yith-ywgc-email-notify-customer.php:155
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:161
msgid "Introductive message"
msgstr ""

#: lib/emails/class-yith-ywgc-email-send-gift-card.php:33
msgid "YITH Gift Cards - Dispatch of the coupon"
msgstr ""

#: lib/emails/class-yith-ywgc-email-send-gift-card.php:36
msgid ""
"Send the digital gift card to the email address selected during the purchase"
msgstr ""

#: lib/emails/class-yith-ywgc-email-send-gift-card.php:39
msgid "Your gift card"
msgstr ""

#: lib/emails/class-yith-ywgc-email-send-gift-card.php:40
msgid "[{site_title}] You have received a gift card"
msgstr ""

#: lib/emails/class-yith-ywgc-email-send-gift-card.php:46
#: lib/emails/class-yith-ywgc-email-send-gift-card.php:79
msgid ""
"Hi {recipient_name}, you have received this gift card from {sender}, use it "
"on our online shop."
msgstr ""

#: lib/emails/class-yith-ywgc-email-send-gift-card.php:81
msgid "a friend"
msgstr ""

#: plugin-options/general-options.php:19
msgid "General settings"
msgstr ""

#: plugin-options/general-options.php:23
msgid "Allow variable amount"
msgstr ""

#: plugin-options/general-options.php:26
msgid ""
"Allow your customers to add any amount in addition to those already set."
msgstr ""

#: plugin-options/general-options.php:30
msgid "Enable the \"Gift this product\" option"
msgstr ""

#: plugin-options/general-options.php:33
msgid ""
"Allow users to create a gift card from the product page and suggest this "
"product in the gift-card email."
msgstr ""

#: plugin-options/general-options.php:37
msgid "Allow editing"
msgstr ""

#: plugin-options/general-options.php:40
msgid "Allow your users to edit the message and the receiver of the gift card"
msgstr ""

#: plugin-options/general-options.php:44
msgid "Purchase notification"
msgstr ""

#: plugin-options/general-options.php:47
msgid "Notify customers when a gift card they have purchased is used"
msgstr ""

#: plugin-options/general-options.php:51
msgid "Enable send later"
msgstr ""

#: plugin-options/general-options.php:54
msgid "Let your customer to set a delivery date for the gift cards purchased"
msgstr ""

#: plugin-options/general-options.php:58
msgid "Recipient email is mandatory"
msgstr ""

#: plugin-options/general-options.php:61
msgid "Choose if the recipient email is mandatory for digital gift cards."
msgstr ""

#: plugin-options/general-options.php:65
msgid "BCC email"
msgstr ""

#: plugin-options/general-options.php:68
msgid ""
"Send the email containing the gift card code to the admin with Blind Carbon "
"Copy"
msgstr ""

#: plugin-options/general-options.php:72
msgid "Link to auto discount"
msgstr ""

#: plugin-options/general-options.php:75
msgid ""
"Let the customer click the link in the received email in order to have the "
"discount applied automatically in the cart"
msgstr ""

#: plugin-options/general-options.php:86
msgid "Allow custom design"
msgstr ""

#: plugin-options/general-options.php:89
msgid ""
"Allow your users to upload a custom picture to be used as the gift card "
"design"
msgstr ""

#: plugin-options/general-options.php:93
msgid "Allow template design"
msgstr ""

#: plugin-options/general-options.php:96
msgid "Allow your users to choose from a selection of templates."
msgstr ""

#: plugin-options/general-options.php:97
msgid "Set your template categories"
msgstr ""

#: plugin-options/general-options.php:101
msgid "Allow multiple recipients"
msgstr ""

#: plugin-options/general-options.php:104
msgid "Allows you to set multiple recipients for single gift cards"
msgstr ""

#: plugin-options/general-options.php:108
msgid "Action to perform on order cancelled"
msgstr ""

#: plugin-options/general-options.php:111
msgid ""
"Choose what happens to gift cards purchased on an order that is set as "
"cancelled"
msgstr ""

#: plugin-options/general-options.php:113
#: plugin-options/general-options.php:125
msgid "Do nothing"
msgstr ""

#: plugin-options/general-options.php:114
#: plugin-options/general-options.php:126
msgid "Disable the gift cards"
msgstr ""

#: plugin-options/general-options.php:115
#: plugin-options/general-options.php:127
msgid "Dismiss the gift cards"
msgstr ""

#: plugin-options/general-options.php:120
msgid "Action to perform on order refunded"
msgstr ""

#: plugin-options/general-options.php:123
msgid ""
"Choose what happens to gift cards purchased on an order that is set as "
"refunded"
msgstr ""

#: plugin-options/general-options.php:132
msgid "Pre-printed physical gift cards"
msgstr ""

#: plugin-options/general-options.php:135
msgid ""
"Choose if the physical gift cards are pre-printed. In this case the gift "
"card code will not be generated automatically"
msgstr ""

#: plugin-options/general-options.php:140
msgctxt ""
"Option(Title): enable the gift cards to apply discount to shipping costs"
msgid "Shipping discount"
msgstr ""

#: plugin-options/general-options.php:143
msgctxt ""
"Option(Description): enable the gift cards to apply discount to shipping "
"costs"
msgid "Apply gift cards balance for shipping cost discount"
msgstr ""

#: plugin-options/general-options.php:152
msgid "Make the gift card expire"
msgstr ""

#: plugin-options/general-options.php:153
msgid ""
"Choose whether to make the gift card expire or not. Set the number of months "
"it is valid for since the date it is purchased. Set 0 to make it never "
"expire."
msgstr ""

#: plugin-options/general-options.php:162
msgid "Show recipient email in cart details"
msgstr ""

#: plugin-options/general-options.php:163
msgid ""
"Choose whether to show the recipient email of the gift card on cart details."
msgstr ""

#: plugin-options/general-options.php:169
msgid "Show preset title"
msgstr ""

#: plugin-options/general-options.php:170
msgid ""
"Choose whether to show the image title next to the preset images when the "
"user choose to select a design."
msgstr ""

#: plugin-options/template-options.php:19
msgid "Template settings"
msgstr ""

#: plugin-options/template-options.php:23
msgid "Shop name"
msgstr ""

#: plugin-options/template-options.php:26
msgid "Set the name of the shop for the email sent to the customer."
msgstr ""

#: plugin-options/template-options.php:29
msgid "Max image size"
msgstr ""

#: plugin-options/template-options.php:32
msgid ""
"Put a limit (in MB) to the size of the customized images that customers can "
"use. Set to 0 if you don't want any limit."
msgstr ""

#: plugin-options/template-options.php:41
msgid "Shop logo"
msgstr ""

#: plugin-options/template-options.php:44
msgid ""
"Set the logo of the shop you want to show in the gift card sent to "
"customers. The logo will be showed with a maximum size of 100x60 pixels."
msgstr ""

#: plugin-options/template-options.php:47
msgid "Shop logo in gift card"
msgstr ""

#: plugin-options/template-options.php:50
msgid ""
"Set if the shop logo should be shown on the gift card template. Disable it "
"if for example, your gift cards template image contains your shop logo"
msgstr ""

#: plugin-options/template-options.php:54
msgid "Logo of the gift card product"
msgstr ""

#: plugin-options/template-options.php:57
msgid "Select the logo of a default gift card product"
msgstr ""

#: plugin-options/template-options.php:60
msgid "Template style"
msgstr ""

#: plugin-options/template-options.php:63
msgid "Style 1"
msgstr ""

#: plugin-options/template-options.php:64
msgid "Style 2"
msgstr ""

#: plugin-options/template-options.php:68
msgid "Select the style of the gift card template"
msgstr ""

#: templates/checkout/form-gift-cards.php:19
msgid "Have a gift card?"
msgstr ""

#: templates/checkout/form-gift-cards.php:19
msgid "Click here to enter your code"
msgstr ""

#: templates/checkout/form-gift-cards.php:30
msgid "Gift card code"
msgstr ""

#: templates/checkout/form-gift-cards.php:33
msgid "Apply gift card"
msgstr ""

#: templates/emails/automatic-discount.php:14
msgid ""
"In order to use this gift card you can enter the gift card code in the "
"appropriate field of the cart page or you can click the following link to "
"obtain the discount automatically."
msgstr ""

#: templates/emails/automatic-discount.php:18
msgid "Click here for the discount"
msgstr ""

#: templates/emails/product-suggestion.php:15
#, php-format
msgid ""
"%s would like to suggest you to use this gift card to purchase the following "
"product:"
msgstr ""

#: templates/emails/product-suggestion.php:29
msgid "Go to the product"
msgstr ""

#: templates/myaccount/my-giftcards.php:14
msgid "Code"
msgstr ""

#: templates/myaccount/my-giftcards.php:15
msgid "Value"
msgstr ""

#: templates/myaccount/my-giftcards.php:17
msgid "Usage"
msgstr ""

#: templates/myaccount/my-giftcards.php:18
msgid "Status"
msgstr ""

#: templates/myaccount/my-giftcards.php:35
msgid "My gift cards"
msgstr ""

#: templates/myaccount/my-giftcards.php:79
#, php-format
msgctxt "gift card expiration date"
msgid "Expire on: %s (Y-m-d)"
msgstr ""

#: templates/myaccount/my-giftcards.php:91
#, php-format
msgid "Order %s"
msgstr ""

#: templates/single-product/add-to-cart/gift-card.php:27
msgid "This product cannot be purchased"
msgstr ""

#: templates/single-product/add-to-cart/give-product-as-present.php:16
msgid "Gift this product"
msgstr ""

#: templates/yith-gift-cards/gift-card-design.php:16
msgid "Gift card design"
msgstr ""

#: templates/yith-gift-cards/gift-card-design.php:22
msgid "Default image"
msgstr ""

#: templates/yith-gift-cards/gift-card-design.php:30
msgid "Customize"
msgstr ""

#: templates/yith-gift-cards/gift-card-design.php:39
#: templates/yith-gift-cards/gift-card-presets.php:52
msgid "Choose design"
msgstr ""

#: templates/yith-gift-cards/gift-card-details.php:19
msgid "Recipient's email (*)"
msgstr ""

#: templates/yith-gift-cards/gift-card-details.php:21
msgid "Recipient's email"
msgstr ""

#: templates/yith-gift-cards/gift-card-details.php:31
msgid "If empty, will be sent to your email address"
msgstr ""

#: templates/yith-gift-cards/gift-card-details.php:39
msgid "Add another recipient"
msgstr ""

#: templates/yith-gift-cards/gift-card-details.php:48
msgid "Your name"
msgstr ""

#: templates/yith-gift-cards/gift-card-details.php:61
msgid "Postpone delivery"
msgstr ""

#: templates/yith-gift-cards/gift-card-details.php:64
msgid "Choose the delivery date (year-month-day)"
msgstr ""

#: templates/yith-gift-cards/gift-card-presets.php:21
msgid "Show all design"
msgstr ""

#: templates/yith-gift-cards/gift-card-template.php:21
#: templates/yith-gift-cards/gift-card-template.php:22
#: templates/yith-gift-cards/gift-card-template.php:43
#: templates/yith-gift-cards/gift-card-template.php:44
msgid "The shop logo for the gift card"
msgstr ""

#: templates/yith-gift-cards/gift-card-template.php:53
msgid "Your Gift Card code:"
msgstr ""
