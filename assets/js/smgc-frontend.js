;(function ($) {

    var init_plugin = function () {
        if (jQuery().prettyPhoto) {
            $('[rel="prettyPhoto[smgc-choose-design]"]').prettyPhoto({

                hook              : 'rel',
                social_tools      : false,
                theme             : 'pp_woocommerce',
                default_width     : '80%',
                default_height    : '100%',
                horizontal_padding: 20,
                opacity           : 0.8,
                deeplinking       : false,
                keyboard_shortcuts: true,
                allow_resize      : false
            });
        }
    };

    $(document).ready(function () {
        /** init datepicker */
        $("div.smgc-generator .datepicker").datepicker({dateFormat: "yy-mm-dd", minDate: +1, maxDate: "+1Y"});
    });

    var hide_on_gift_as_present = function () {
        if ($('input[name="smgc-as-present-enabled"]').length) {
            $('.smgc-generator').hide();
            show_gift_card_editor(false);
        }
    }

    init_plugin();

    show_hide_add_to_cart_button();

    hide_on_gift_as_present();

    $(document).on('click', '.smgc-choose-image.smgc-choose-template', function (e) {
        init_plugin();
    });

    $(document).on('click', '.pp_content_container .smgc-choose-preset', function (e) {
        var id = $(this).data('design-id');
        var design_url = $(this).data('design-url');
        $('#smgc-main-image').attr('src', design_url);
        $(document).trigger('smgc-picture-changed', ['template', id]);
        $.prettyPhoto.close();
    });

    $(document).on('click', 'a.smgc-show-category', function (e) {

        var current_category = $(this).data("category-id");

        //  highlight the selected category
        $('a.smgc-show-category').removeClass('smgc-category-selected');
        $(this).addClass('smgc-category-selected');

        //  Show only the design of the selected category
        if ('all' !== current_category) {
            $('.smgc-design-item').hide();
            $('.smgc-design-item.' + current_category).show();
        }
        else {
            $('.smgc-design-item').show();

        }
        return false;
    });

    $(document).on('smgc-picture-changed', function (event, type, id) {

            $('#smgc-design-type').val(type);
            $('#smgc-template-design').val(id);
        }
    );


    $(document).on('click', 'a.smgc-show-giftcard', show_coupon_form);

    function show_coupon_form() {
        $('.smgc-enter-code').slideToggle(400, function () {
            $('.smgc-enter-code').find(':input:eq(0)').focus();
        });
        return false;
    }

    /** Show the edit gift card button */
    $("button.smgc-do-edit").css("display", "inline");


    function update_gift_card_amount(amount) {

        $("div.smgc-card-amount span.amount").text(amount);

    }

    function show_gift_card_editor(val) {
        $('button.gift_card_add_to_cart_button').attr('disabled', !val);


    }

    function show_hide_add_to_cart_button() {
        var select_element = $(".gift-cards-list select");
        var gift_this_product = $('#give-as-present');

        if (!gift_this_product.length) {
            $('.gift-cards-list input.smgc-manual-amount').addClass('smgc-hidden');
            $('.smgc-manual-amount-error').remove();

            var amount = 0;
            if ((select_element.length == 0) || ("-1" == select_element.val())) {
                /* the user should enter a manual value as gift card amount */
                var manual_amount_element = $('.gift-cards-list input.smgc-manual-amount');
                if (manual_amount_element.length) {
                    var manual_amount = manual_amount_element.val();
                    manual_amount_element.removeClass('smgc-hidden');

                    var test_amount = new RegExp('^[1-9]\\d*(?:' + '\\' + smgc_data.currency_format_decimal_sep + '\\d{1,2})?$', 'g')

                    if (manual_amount.length && !test_amount.test(manual_amount)) {
                        manual_amount_element.after('<div class="smgc-manual-amount-error">' + smgc_data.manual_amount_wrong_format + '</div>');
                        show_gift_card_editor(false);
                    }
                    else {
                        /** If the user entered a valid amount, show "add to cart" button and gift card
                         *  editor.
                         */
                        if (manual_amount) {
                            // manual amount is a valid numeric value
                            show_gift_card_editor(true);

                            amount = accounting.unformat(manual_amount, smgc_data.mon_decimal_point);

                            if (amount <= 0) {
                                show_gift_card_editor(false);
                            }
                            else {
                                amount = accounting.formatMoney(amount, {
                                    symbol   : smgc_data.currency_format_symbol,
                                    decimal  : smgc_data.currency_format_decimal_sep,
                                    thousand : smgc_data.currency_format_thousand_sep,
                                    precision: smgc_data.currency_format_num_decimals,
                                    format   : smgc_data.currency_format
                                });

                                show_gift_card_editor(true);
                            }
                        }
                        else {
                            show_gift_card_editor(false);
                        }
                    }
                }
            }
            else if (!select_element.val()) {
                show_gift_card_editor(false);
            }
            else {
                show_gift_card_editor(true);
                amount = accounting.formatMoney(select_element.children("option:selected").data('price'), {
                    symbol   : smgc_data.currency_format_symbol,
                    decimal  : smgc_data.currency_format_decimal_sep,
                    thousand : smgc_data.currency_format_thousand_sep,
                    precision: smgc_data.currency_format_num_decimals,
                    format   : smgc_data.currency_format
                });
                //amount = select_element.children("option:selected").text();
            }

            update_gift_card_amount(amount);
        }
    }

    $(document).on('input', '.gift-cards-list input.smgc-manual-amount', function (e) {
        show_hide_add_to_cart_button();
    });

    function add_recipient() {
        var last = $('div.smgc-single-recipient').last();
        var required = smgc_data.mandatory_email ? 'required' : '';
        var new_div = '<div class="smgc-single-recipient">' +
            '<input type="email" name="smgc-recipient-email[]" class="smgc-recipient" ' + required + '/>' +
            '<a href="#" class="smgc-remove-recipient hide-if-alone">x</a> ' +
            '</div>';

        last.after(new_div);

        //  show the remove recipient links
        $("a.smgc-remove-recipient").css('visibility', 'visible');

        $("div.gift_card_template_button input[name='quantity']").css("display", "none");

        //  show a message for quantity disabled when multi recipients is entered
        if (!$("div.gift_card_template_button div.smgc-multi-recipients").length) {
            $("div.gift_card_template_button div.quantity").after("<div class='smgc-multi-recipients'><span>" + smgc_data.multiple_recipient + "</span></div>");
        }
    }

    function remove_recipient(element) {
        //  remove the element
        $(element).parent("div.smgc-single-recipient").remove();

        //  Avoid the deletion of all recipient
        var emails = $('input[name="smgc-recipient-email[]"');
        if (emails.length == 1) {
            //  only one recipient is entered...
            $("a.hide-if-alone").css('visibility', 'hidden');
            $("div.gift_card_template_button input[name='quantity']").css("display", "inherit");

            $("div.smgc-multi-recipients").remove();
        }
    }

    $(document).on('click', 'a.add-recipient', function (e) {
        e.preventDefault();
        add_recipient();
    });

    $(document).on('click', 'a.smgc-remove-recipient', function (e) {
        e.preventDefault();
        remove_recipient($(this));
    });

    $(document).on('input', '#smgc-recipient-name', function (e) {
        $(".smgc-card-rname").html($('#smgc-recipient-name').val());
    });

    $(document).on('input', '#smgc-edit-message', function (e) {
        $(".smgc-card-message").html($('#smgc-edit-message').val());
    });

    $(document).on('change', '.gift-cards-list select', function (e) {
        show_hide_add_to_cart_button();
    });

    $(document).on('click', 'a.customize-gift-card', function (e) {
        e.preventDefault();
        $('div.summary.entry-summary').after('<div class="smgc-customizer"></div>');
    });

    /** Set to default the image used on the gift card editor on product page */
    $(document).on('click', '.smgc-default-picture', function (e) {
        e.preventDefault();
        var control = $('#smgc-upload-picture');
        control.replaceWith(control = control.clone(true));
        $('.smgc-main-image img.smgc-main-image').attr('src', smgc_data.default_gift_card_image);

        //  Reset style if previously a custom image was used
        $(".smgc-main-image").css("background-color", "");
        $("div.gift-card-too-small").remove();
        $(document).trigger('smgc-picture-changed', ['default']);
    });

    /** Show the custom file choosed by the user as the image used on the gift card editor on product page */
    $(document).on('click', '.smgc-custom-picture', function (e) {
        $('#smgc-upload-picture').click();
    });

    $('#smgc-upload-picture').on('change', function () {
        var preview_image = function (file) {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(file);

            oFReader.onload = function (oFREvent) {
                document.getElementById("smgc-main-image").src = oFREvent.target.result;
                $(document).trigger('smgc-picture-changed', ['custom']);

                /** Check the size of the file choosed and notify it to the user if it is too small */
                if ($("#smgc-main-image").width() < $(".smgc-main-image").width()) {
                    $(".smgc-main-image").css("background-color", "#ffe326");
                    $(".smgc-preview").prepend('<div class="gift-card-too-small">' + smgc_data.notify_custom_image_small + '</div>');
                }
                else {
                    $(".smgc-main-image").css("background-color", "");
                }
            }
        }

        //  Remove previous errors shown
        $(".smgc-picture-error").remove();

        var ext = $(this).val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'bmp']) == -1) {
            $("div.gift-card-content-editor.step-appearance").append('<span class="smgc-picture-error">' +
                smgc_data.invalid_image_extension + '</span>');
            return;
        }

        if ($(this)[0].files[0].size > smgc_data.custom_image_max_size * 1024 * 1024) {
            $("div.gift-card-content-editor.step-appearance").append('<span class="smgc-picture-error">' +
                smgc_data.invalid_image_size + '</span>');
            return;
        }

        preview_image($(this)[0].files[0]);
    });

    $(document).on('click', '#give-as-present', function (e) {

        e.preventDefault();
        $("#give-as-present").css("visibility", "hidden");


        $("div.smgc-generator").append('<input type="hidden" name="smgc-as-present" value="1">');
        $("div.smgc-generator").prependTo($('form.cart'));
        $('button.single_add_to_cart_button').data('add-to-cart-text', $('button.single_add_to_cart_button').text());
        $('button.single_add_to_cart_button').html('Add gift');


        $("#smgc-cancel-gift-card").css("visibility", "visible");
        $("div.smgc-generator").css('display', 'inherit');
        $("div.smgc-generator").css('visibility', 'visible');
    });

    $(document).on('click', '#smgc-cancel-gift-card', function (e) {
        e.preventDefault();
        $("div.smgc-generator input[name='smgc-as-present']").remove();

        $('button.single_add_to_cart_button').html($('button.single_add_to_cart_button').data('add-to-cart-text'));

        $("#give-as-present").css("visibility", "visible");
        $("#smgc-cancel-gift-card").css("visibility", "hidden");

        $("div.smgc-generator").css('display', 'none');
    });

    $(document).on('change', '#smgc-postdated', function (e) {
        if ($(this).is(':checked')) {
            $("#smgc-delivery-date").removeClass("smgc-hidden");
        }
        else {
            $("#smgc-delivery-date").addClass("smgc-hidden");
        }
    });

    function set_giftcard_value(value) {
        $("div.smgc-card-amount span.amount").html(value);
    }

    $(document).on('found_variation', function (ev, variation) {

        // $form = $('form.variations_form');
        // $product_variations = $form.data( 'product_variations' );

        // var variation_price_element = $(".product .entry-summary .price .amount");
        // if (variation_price_element.length) {
        set_giftcard_value($(variation.price_html).html());
        // }
    });


    $(document).on('reset_data', function () {
        set_giftcard_value('');
    });

    function show_edit_gift_cards(element, visible) {
        var container = $(element).closest("div.smgc-gift-card-content");
        var edit_container = container.find("div.smgc-gift-card-edit-details");
        var details_container = container.find("div.smgc-gift-card-details");

        if (visible) {
            //go to edit
            edit_container.removeClass("smgc-hide");
            edit_container.addClass("smgc-show");

            details_container.removeClass("smgc-show");
            details_container.addClass("smgc-hide");
        }
        else {
            //go to details
            edit_container.removeClass("smgc-show");
            edit_container.addClass("smgc-hide");

            details_container.removeClass("smgc-hide");
            details_container.addClass("smgc-show");
        }
    }

    $(document).on('click', 'button.smgc-apply-edit', function (e) {

        var clicked_element = $(this);

        var container = clicked_element.closest("div.smgc-gift-card-content");

        var sender = container.find('input[name="smgc-edit-sender"]').val();
        var recipient = container.find('input[name="smgc-edit-recipient"]').val();
        var message = container.find('textarea[name="smgc-edit-message"]').val();
        var item_id = container.find('input[name="smgc-item-id"]').val();

        var gift_card_element = container.find('input[name="smgc-gift-card-id"]');
        var gift_card_id = gift_card_element.val();

        //  Apply changes, if apply button was clicked
        if (clicked_element.hasClass("apply")) {
            var data = {
                'action'      : 'edit_gift_card',
                'gift_card_id': gift_card_id,
                'item_id'     : item_id,
                'sender'      : sender,
                'recipient'   : recipient,
                'message'     : message
            };

            container.block({
                message   : null,
                overlayCSS: {
                    background: "#fff url(" + smgc_data.loader + ") no-repeat center",
                    opacity   : .6
                }
            });

            $.post(smgc_data.ajax_url, data, function (response) {
                if (response.code > 0) {
                    container.find("span.smgc-sender").text(sender);
                    container.find("span.smgc-recipient").text(recipient);
                    container.find("span.smgc-message").text(message);

                    if (response.code == 2) {
                        gift_card_element.val(response.values.new_id);
                    }
                }

                container.unblock();

                //go to details
                show_edit_gift_cards(clicked_element, false);
            });
        }
    });

    $(document).on('click', 'button.smgc-cancel-edit', function (e) {

        var clicked_element = $(this);

        //go to details
        show_edit_gift_cards(clicked_element, false);
    });

    $(document).on('click', 'button.smgc-do-edit', function (e) {

        var clicked_element = $(this);
        //go to edit
        show_edit_gift_cards(clicked_element, true);
    });

    $(document).on('click', 'form.gift-cards_form button.gift_card_add_to_cart_button', function (e) {
        $('div.gift-card-content-editor.step-content p.smgc-filling-error').remove();
        if ($('#smgc-postdated').is(':checked') && !$.datepicker.parseDate('yy-mm-dd', $('#smgc-delivery-date').val())) {
            $('div.gift-card-content-editor.step-content').append('<p class="smgc-filling-error">' + smgc_data.missing_scheduled_date + '</p>');
            e.preventDefault();
        }
    });

    $(document).on('click', '.smgc-gift-card-content a.edit-details', function (e) {
        e.preventDefault();
        $(this).addClass('smgc-hide');
        $('div.smgc-gift-card-details').toggleClass('smgc-hide');
    });


    $('.smgc-single-recipient input[name="smgc-recipient-email[]"]').each(function (i, obj) {
        $(this).on('input', function () {
            $(this).closest('.smgc-single-recipient').find('.smgc-bad-email-format').remove();
        });
    });

    function validateEmail(email) {
        var test_email = new RegExp('^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,}$', 'i');
        return test_email.test(email);
    }

    $(document).on('submit', '.gift-cards_form', function (e) {
        var can_submit = true;
        $('.smgc-single-recipient input[name="smgc-recipient-email[]"]').each(function (i, obj) {

            if ($(this).val() && !validateEmail($(this).val())) {
                $(this).closest('.smgc-single-recipient').find('.smgc-bad-email-format').remove();
                $(this).after('<span class="smgc-bad-email-format">' + smgc_data.email_bad_format + '</span>');
                can_submit = false;
            }
        });
        if (!can_submit) {
            e.preventDefault();
        }
    });
    /** Manage the WooCommerce 2.6 changes in the cart template
     * with AJAX
     * @since 1.4.0
     */

    /**
     * Apply the gift card code the same way WooCommerce do for Coupon code
     *
     * @param {JQuery Object} $form The cart form.
     */
    $('form.smgc-enter-code').submit(function (e) {
        block($(this));

        var $form = $(this);
        var $text_field = $form.find('input[name="coupon_code"]');
        var coupon_code = $text_field.val();

        var data = {
            security    : smgc_data.apply_coupon_nonce,
            is_gift_card: 1,
            coupon_code : coupon_code
        };

        $.ajax({
            type    : 'POST',
            url     : get_url('apply_coupon'),
            data    : data,
            dataType: 'html',
            success : function (response) {
                show_notice(response);
                $(document.body).trigger('applied_coupon');
            },
            complete: function () {
                unblock($form);
                $text_field.val('');
                update_cart_totals();
            }
        });
    });

    /**
     * Block a node visually for processing.
     *
     * @param {JQuery Object} $node
     */
    var block = function ($node) {
        $node.addClass('processing').block({
            message   : null,
            overlayCSS: {
                background: '#fff',
                opacity   : 0.6
            }
        });
    };

    /**
     * Unblock a node after processing is complete.
     *
     * @param {JQuery Object} $node
     */
    var unblock = function ($node) {
        $node.removeClass('processing').unblock();
    };

    /**
     * Gets a url for a given AJAX endpoint.
     *
     * @param {String} endpoint The AJAX Endpoint
     * @return {String} The URL to use for the request
     */
    var get_url = function (endpoint) {
        return smgc_data.wc_ajax_url.toString().replace(
            '%%endpoint%%',
            endpoint
        );
    };

    /**
     * Clear previous notices and shows new one above form.
     *
     * @param {Object} The Notice HTML Element in string or object form.
     */
    var show_notice = function (html_element, $target) {
        if (!$target) {
            $target = $('table.shop_table.cart').closest('form');
        }
        $('.woocommerce-error, .woocommerce-message').remove();
        $target.before(html_element);
    };

    /**
     * Update the cart after something has changed.
     */
    function update_cart_totals() {
        block($('div.cart_totals'));

        $.ajax({
            url     : get_url('get_cart_totals'),
            dataType: 'html',
            success : function (response) {
                $('div.cart_totals').replaceWith(response);
            }
        });
    }

    /**
     * Integration with SMMS Quick View and some third party themes
     */
    $(document).on('qv_loader_stop yit_quick_view_loaded flatsome_quickview', function () {

        show_hide_add_to_cart_button();

        hide_on_gift_as_present();

    });


})(jQuery);