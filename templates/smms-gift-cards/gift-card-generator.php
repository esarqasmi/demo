<?php
/**
 * Gift Card product add to cart
 *
 * @author  Yithemes
 * @package SMMS WooCommerce Gift Cards
 *
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>
	<div class="smgc-generator">

		<input type="hidden" name="smgc-is-digital" value="1" />

		<?php if ( ! ( $product instanceof WC_Product_Gift_Card ) ): ?>
			<input type="hidden" name="smgc-as-present-enabled" value="1">
		<?php endif; ?>

		<?php do_action( 'smms_smgc_gift_card_preview', $product ); ?>

		<div class="gift-card-content-editor variations_button">

			<?php do_action( 'smms_smgc_gift_card_preview_content', $product ); ?>

			<?php do_action( 'smms_smgc_generator_buttons_before', $product ); ?>

		</div>
	</div>
<?php
do_action( 'smms_smgc_gift_card_preview_end', $product );
