<?php
/**
 * Gift Card product add to cart
 *
 * @author  Yithemes
 * @package SMMS WooCommerce Gift Cards
 *
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div class="smgc-template <?php echo $template_style; ?>">

	<?php
	/** Show company logo on top of the template if 'style2' is set */
	if ( $company_logo_url && ( 'style2' == $template_style ) ) : ?>
		<div class="smgc-top-header">
			<img src="<?php echo $company_logo_url; ?>"
			     class="smgc-logo-shop-image"
			     alt="<?php _e( "The shop logo for the gift card", 'smms-woocommerce-gift-cards' ); ?>"
			     title="<?php _e( "The shop logo for the gift card", 'smms-woocommerce-gift-cards' ); ?>">
		</div>
	<?php endif; ?>

	<div class="smgc-preview">
		<div class="smgc-main-image">
			<?php if ( $header_image_url ): ?>
				<img src="<?php echo $header_image_url; ?>"
				     id="smgc-main-image" class="smgc-main-image"
				     alt="<?php _e( "Gift card image", 'smms-woocommerce-gift-cards' ); ?>"
				     title="<?php _e( "Gift card image", 'smms-woocommerce-gift-cards' ); ?>">
			<?php endif; ?>

		</div>
		<div class="smgc-card-values">
			<?php
			/** Show company logo under the main image if 'style1' is set */
			if ( $company_logo_url && ( 'style1' == $template_style ) ) : ?>
				<div class="smgc-logo-shop">
					<img src="<?php echo $company_logo_url; ?>"
					     class="smgc-logo-shop-image"
					     alt="<?php _e( "The shop logo for the gift card", 'smms-woocommerce-gift-cards' ); ?>"
					     title="<?php _e( "The shop logo for the gift card", 'smms-woocommerce-gift-cards' ); ?>">
				</div>
			<?php endif; ?>

			<div class="smgc-card-amount">
				<?php echo $formatted_price; ?>
			</div>
		</div>
		<div class="smgc-card-code">
			<?php _e( "Your Gift Card code:", 'smms-woocommerce-gift-cards' ); ?>
			<span class="smgc-generated-code"><?php echo $gift_card_code; ?></span>
		</div>
		<div class="smgc-card-rname"><?php echo $amount ; ?></div>
		<div class="smgc-card-message"><?php echo $message; ?></div>
        
	</div>
</div>
