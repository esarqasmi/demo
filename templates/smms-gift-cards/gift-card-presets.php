<?php
/**
 * Gift Card product add to cart
 *
 * @author  Yithemes
 * @package SMMS WooCommerce Gift Cards
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div id="smgc-choose-design" class="smgc-template-design" style="display: none">
	<div>
		<?php if ( count( $categories ) > 1 ): ?>
			<ul class="smgc-template-categories">
				<li class="smgc-template-item smgc-category-all">
					<a href="#" class="smgc-show-category smgc-category-selected"
					   data-category-id="all">
						<?php _e( "Show all design", 'smms-woocommerce-gift-cards' ); ?>
					</a>
				</li>
				<?php foreach ( $categories as $item ): ?>
					<li class="smgc-template-item smgc-category-<?php echo $item->term_id; ?>">
						<a href="#" class="smgc-show-category"
						   data-category-id="smgc-category-<?php echo $item->term_id; ?>">
							<?php echo $item->name; ?>
						</a>
					</li>

				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<div class="smgc-design-list">

			<?php foreach ( $item_categories as $item_id => $categories ): ?>

				<div class="smgc-design-item <?php echo $categories; ?> template-<?php echo $item_id; ?>">

					<div class="smgc-preset-image">
						<?php echo wp_get_attachment_image( intval( $item_id ), 'shop_catalog' ); ?>
						<?php if ( SMMS_SMGC()->show_preset_title ):
							$post = get_post( $item_id );
							if ( $post ): ?>
								<span class="smgc-preset-title"><?php echo $post->post_title; ?></span>
							<?php endif; ?>
						<?php endif; ?>
					</div>
					<button class="smgc-choose-preset"
					        data-design-id="<?php echo $item_id; ?>"
					        data-design-url="<?php echo smms_get_attachment_image_url( intval( $item_id ), 'full' ); ?>"><?php _e( "Choose design", 'smms-woocommerce-gift-cards' ); ?></button>
				</div>

			<?php endforeach; ?>

		</div>
	</div>
</div>
