<?php
/**
 * Gift Card product add to cart
 *
 * @author  Yithemes
 * @package SMMS WooCommerce Gift Cards
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $allow_templates || $allow_customer_images ) : ?>
	<div class="gift-card-content-editor step-appearance">
				<span class="smgc-editor-section-title">
								<?php _e( "Gift card design", 'smms-woocommerce-gift-cards' ); ?>
							</span>

		<!-- Let the user to cancel a selection, turning back to the default design -->
		<input type="button"
		       class="smgc-choose-image smgc-default-picture"
		       value="<?php _e( "Default image", 'smms-woocommerce-gift-cards' ); ?>" />
		<input type="hidden" name="smgc-design-type" id="smgc-design-type" value="default" />
		<input type="hidden" name="smgc-template-design" id="smgc-template-design" value="-1" />

		<!-- Let the user to upload a file to be used as gift card main image -->
		<?php if ( $allow_customer_images ) : ?>
			<input type="button"
			       class="smgc-choose-image smgc-custom-picture"
			       value="<?php _e( "Customize", 'smms-woocommerce-gift-cards' ); ?>" />
			<input type="file" name="smgc-upload-picture" id="smgc-upload-picture"
			       accept="image/*" />
		<?php endif; ?>
		<?php if ( $allow_templates ) : ?>
			<input type="button"
			       class="smgc-choose-image smgc-choose-template"
			       href="#smgc-choose-design"
			       rel="prettyPhoto[smgc-choose-design]"
			       value="<?php _e( "Choose design", 'smms-woocommerce-gift-cards' ); ?>" />

		<?php endif; ?>
	</div>
<?php endif;