<?php
/**
 * Gift Card product add to cart
 *
 * @author  Yithemes
 * @package SMMS WooCommerce Gift Cards
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div class="gift-card-content-editor step-content">
	<span class="smgc-editor-section-title"><?php _e( "Gift card details", 'smms-woocommerce-gift-cards' ); ?></span>

	<label
		for="smgc-recipient-email">
		<?php if ( $mandatory_recipient ) {
			_e( "Recipient's email (*)", 'smms-woocommerce-gift-cards' );
		} else {
			_e( "Recipient's email", 'smms-woocommerce-gift-cards' );
		}
		?></label>

	<div class="smgc-single-recipient">
		<input type="email"
		       name="smgc-recipient-email[]" <?php echo ( $mandatory_recipient && ! $gift_this_product ) ? 'required' : ''; ?>
		       class="smgc-recipient" />
		<a href="#" class="smgc-remove-recipient hide-if-alone">x</a>
		<?php if ( ! $mandatory_recipient ): ?>
			<span class="smgc-empty-recipient-note"><?php _e( "If empty, will be sent to your email address", 'smms-woocommerce-gift-cards' ); ?></span>
		<?php endif; ?>
	</div>

	<?php
	//Only with gift card product type you can use multiple recipients
	if ( $allow_multiple_recipients ) : ?>
		<a href="#" class="add-recipient"
		   id="add_recipient"><?php _e( "Add another recipient", 'smms-woocommerce-gift-cards' ); ?></a>
	<?php endif; ?>
	<div class="smgc-recipient-name">
		<label
			for="smgc-recipient-name"><?php _e( "Recipient name", 'smms-woocommerce-gift-cards' ); ?></label>
		<input type="text" name="smgc-recipient-name" id="smgc-recipient-name">
	</div>
	<div class="smgc-sender-name">
		<label
			for="smgc-sender-name"><?php _e( "Your name", 'smms-woocommerce-gift-cards' ); ?></label>
		<input type="text" name="smgc-sender-name" id="smgc-sender-name">
	</div>
	<div class="smgc-message">
		<label
			for="smgc-edit-message"><?php _e( "Message", 'smms-woocommerce-gift-cards' ); ?></label>
		<textarea id="smgc-edit-message" name="smgc-edit-message" rows="5"
		          placeholder="<?php _e( "Your message...", 'smms-woocommerce-gift-cards' ); ?>"></textarea>
	</div>

	<?php if ( $allow_send_later ) : ?>
		<div class="smgc-postdated">
			<label
				for="smgc-postdated"><?php _e( "Postpone delivery", 'smms-woocommerce-gift-cards' ); ?></label>
			<input type="checkbox" id="smgc-postdated" name="smgc-postdated">
			<input type="text" id="smgc-delivery-date" name="smgc-delivery-date"
			       class="datepicker smgc-hidden" placeholder="<?php _e( 'Choose the delivery date (year-month-day)', 'smms-woocommerce-gift-cards' ); ?>">
		</div>
	<?php endif; ?>
</div>