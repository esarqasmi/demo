<?php
/**
 * Show a section for the automatic discount link and description
 *
 * @author SMMSEMES
 * @package smms-woocommerce-gift-cards-premium\templates\emails
 */
if ( ! defined ( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="smgc-add-cart-discount">
                <span
	                class="smgc-discount-message"><?php _e( "In order to use this gift card you can enter the gift card code in the appropriate field of the cart page or you can click the following link to obtain the discount automatically.", 'smms-woocommerce-gift-cards' ); ?></span>

	<div class="smgc-discount-link-section">
		<a class="smgc-discount-link"
		   href="<?php echo $apply_discount_url; ?>"><?php _e( 'Click here for the discount', 'smms-woocommerce-gift-cards' ); ?></a>
	</div>
</div>
