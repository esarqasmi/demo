<?php
/**
 * Show a section with a product suggestion if the gift card was purchased as a gift for a product in the shop
 *
 * @author SMMSEMES
 * @package smms-woocommerce-gift-cards-premium\templates\emails
 */

if ( ! defined ( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="smgc-product-suggested">
	<span class="smgc-suggested-text">
		<?php echo sprintf( __( "%s would like to suggest you to use this gift card to purchase the following product:", 'smms-woocommerce-gift-cards' ), $gift_card->sender_name ); ?>
	</span>

	<div style="overflow: hidden">
		<img class="smgc-product-image"
		     src="<?php echo $product->get_image_id() ? current( wp_get_attachment_image_src( $product->get_image_id(), 'thumbnail' ) ) : wc_placeholder_img_src(); ?>" />

		<div class="smgc-product-description">
			<span class="smgc-product-title"><?php echo $product->post->post_title; ?></span>

			<div
				class="smgc-product-excerpt"><?php echo wp_trim_words( $product->post->post_excerpt, 20 ); ?></div>

			<a class="smgc-product-link" href="<?php echo get_permalink( $product->id ); ?>">
				<?php _e( "Go to the product", 'smms-woocommerce-gift-cards' ); ?></a>
		</div>
	</div>
</div>
