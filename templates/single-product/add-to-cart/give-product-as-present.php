<?php
/**
 * Variable product add to cart
 *
 * @author  Yithemes
 * @package SMMS WooCommerce Gift Cards
 *
 */
if ( ! defined ( 'ABSPATH' ) ) {
    exit;
}

do_action ( 'smms_gift_cards_template_before_add_to_cart_form' );
?>
    <button id="give-as-present"
            class="btn btn-ghost give-as-present"><?php _e ( "Gift this product", 'smms-woocommerce-gift-cards' ); ?></button>
<?php
SMMS_SMGC ()->frontend->show_gift_card_generator ();
