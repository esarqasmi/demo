<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
$general_options = array(

	'general' => array(

		array(
			'name' => __( 'General settings', 'smms-woocommerce-gift-cards' ),
			'type' => 'title',
		),
		'smgc_permit_free_amount'       => array(
			'name'    => __( 'Allow variable amount', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_permit_free_amount',
			'desc'    => __( 'Allow your customers to add any amount in addition to those already set.', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),
		'smgc_permit_its_a_present'     => array(
			'name'    => __( 'Enable the "Gift this product" option', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_permit_its_a_present',
			'desc'    => __( 'Allow users to create a gift card from the product page and suggest this product in the gift-card email.', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),
		'smgc_permit_modification'      => array(
			'name'    => __( 'Allow editing', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_permit_modification',
			'desc'    => __( 'Allow your users to edit the message and the receiver of the gift card', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),
		'smgc_notify_customer'          => array(
			'name'    => __( 'Purchase notification', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_notify_customer',
			'desc'    => __( 'Notify customers when a gift card they have purchased is used', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),
		'smgc_enable_send_later'        => array(
			'name'    => __( 'Enable send later', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_enable_send_later',
			'desc'    => __( 'Let your customer to set a delivery date for the gift cards purchased', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),
		'smgc_recipient_mandatory'      => array(
			'name'    => __( 'Recipient email is mandatory', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_recipient_mandatory',
			'desc'    => __( 'Choose if the recipient email is mandatory for digital gift cards.', 'smms-woocommerce-gift-cards' ),
			'default' => 'yes',
		),
		'smgc_blind_carbon_copy'        => array(
			'name'    => __( 'BCC email', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_blind_carbon_copy',
			'desc'    => __( 'Send the email containing the gift card code to the admin with Blind Carbon Copy', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),
		'smgc_auto_discount'            => array(
			'name'    => __( 'Link to auto discount', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_auto_discount',
			'desc'    => __( 'Let the customer click the link in the received email in order to have the discount applied automatically in the cart', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),/*
        'smgc_restricted_usage'          => array (
            'name'    => __ ( 'Restricted usage', 'smms-woocommerce-gift-cards' ),
            'type'    => 'checkbox',
            'id'      => 'smgc_restricted_usage',
            'desc'    => __ ( 'Choose if the gift card can be used only by the user (identified by the email address) that is the recipient of it', 'smms-woocommerce-gift-cards' ),
            'default' => 'no',
        ),*/
		'smgc_custom_design'            => array(
			'name'    => __( 'Allow custom design', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_custom_design',
			'desc'    => __( 'Allow your users to upload a custom picture to be used as the gift card design', 'smms-woocommerce-gift-cards' ),
			'default' => 'yes',
		),
		'smgc_template_design'          => array(
			'name'    => __( 'Allow template design', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_template_design',
			'desc'    => __( 'Allow your users to choose from a selection of templates.', 'smms-woocommerce-gift-cards' ) .
			             ' <a href="' . admin_url( 'edit-tags.php?taxonomy=giftcard-category&post_type=attachment' ) . '" title="' . __( 'Set your template categories', 'smms-woocommerce-gift-cards' ) . '">' . __( 'Set your template categories', 'smms-woocommerce-gift-cards' ) . '</a>',
			'default' => 'yes',
		),
		'smgc_allow_multi_recipients'   => array(
			'name'    => __( 'Allow multiple recipients', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_allow_multi_recipients',
			'desc'    => __( 'Allows you to set multiple recipients for single gift cards', 'smms-woocommerce-gift-cards' ),
			'default' => 'yes',
		),
		'smgc_order_cancelled_action'   => array(
			'name'    => __( 'Action to perform on order cancelled', 'smms-woocommerce-gift-cards' ),
			'type'    => 'select',
			'id'      => 'smgc_order_cancelled_action',
			'desc'    => __( 'Choose what happens to gift cards purchased on an order that is set as cancelled', 'smms-woocommerce-gift-cards' ),
			'options' => array(
				'nothing' => __( 'Do nothing', 'smms-woocommerce-gift-cards' ),
				'disable' => __( 'Disable the gift cards', 'smms-woocommerce-gift-cards' ),
				'dismiss' => __( 'Dismiss the gift cards', 'smms-woocommerce-gift-cards' ),
			),
			'default' => 'nothing',
		),
		'smgc_order_refunded_action'    => array(
			'name'    => __( 'Action to perform on order refunded', 'smms-woocommerce-gift-cards' ),
			'type'    => 'select',
			'id'      => 'smgc_order_refunded_action',
			'desc'    => __( 'Choose what happens to gift cards purchased on an order that is set as refunded', 'smms-woocommerce-gift-cards' ),
			'options' => array(
				'nothing' => __( 'Do nothing', 'smms-woocommerce-gift-cards' ),
				'disable' => __( 'Disable the gift cards', 'smms-woocommerce-gift-cards' ),
				'dismiss' => __( 'Dismiss the gift cards', 'smms-woocommerce-gift-cards' ),
			),
			'default' => 'nothing',
		),
		'smgc_enable_pre_printed'       => array(
			'name'    => __( 'Pre-printed physical gift cards', 'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'id'      => 'smgc_enable_pre_printed',
			'desc'    => __( 'Choose if the physical gift cards are pre-printed. In this case the gift card code will not be generated automatically', 'smms-woocommerce-gift-cards' ),
			'default' => 'no',
		),
		'smgc_enable_shipping_discount' => array(
			'id'      => 'smgc_enable_shipping_discount',
			'name'    => _x( 'Shipping discount',
				'Option(Title): enable the gift cards to apply discount to shipping costs',
				'smms-woocommerce-gift-cards' ),
			'desc'    => _x( 'Apply gift cards balance for shipping cost discount',
				'Option(Description): enable the gift cards to apply discount to shipping costs',
				'smms-woocommerce-gift-cards' ),
			'type'    => 'checkbox',
			'default' => 'no',
		),

		'smgc_usage_expiration' => array(
			'id'                => 'smgc_usage_expiration',
			'name'              => __( 'Make the gift card expire', 'smms-woocommerce-gift-cards' ),
			'desc'              => __( 'Choose whether to make the gift card expire or not. Set the number of months it is valid for since the date it is purchased. Set 0 to make it never expire.', 'smms-woocommerce-gift-cards' ),
			'type'              => 'number',
			'default'           => 0,
			'custom_attributes' => array(
				'min' => 0,
			)
		),
		'smgc_show_recipient_on_cart' => array(
			'id'                => 'smgc_show_recipient_on_cart',
			'name'              => __( 'Show recipient email in cart details', 'smms-woocommerce-gift-cards' ),
			'desc'              => __( 'Choose whether to show the recipient email of the gift card on cart details.', 'smms-woocommerce-gift-cards' ),
			'type'              => 'checkbox',
			'default'           => 'no',
		),
		'smgc_show_preset_title' => array(
			'id'                => 'smgc_show_preset_title',
			'name'              => __( 'Show preset title', 'smms-woocommerce-gift-cards' ),
			'desc'              => __( 'Choose whether to show the image title next to the preset images when the user choose to select a design.', 'smms-woocommerce-gift-cards' ),
			'type'              => 'checkbox',
			'default'           => 'no',
		),
		array(
			'type' => 'sectionend',
		),
	),
);

return $general_options;


