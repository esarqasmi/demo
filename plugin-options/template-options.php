<?php
/**
 * This file belongs to the YIT Plugin Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined ( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
$template_options = array (

    'template' => array (

        array (
            'name' => __ ( 'Template settings', 'smms-woocommerce-gift-cards' ),
            'type' => 'title',
        ),
        'smgc_shop_name'             => array (
            'name' => __ ( 'Shop name', 'smms-woocommerce-gift-cards' ),
            'type' => 'text',
            'id'   => 'smgc_shop_name',
            'desc' => __ ( 'Set the name of the shop for the email sent to the customer.', 'smms-woocommerce-gift-cards' ),
        ),
        'smgc_custom_image_max_size' => array (
            'name'              => __ ( 'Max image size', 'smms-woocommerce-gift-cards' ),
            'type'              => 'number',
            'id'                => 'smgc_custom_image_max_size',
            'desc'              => __ ( 'Put a limit (in MB) to the size of the customized images that customers can use. Set to 0 if you don\'t want any limit.', 'smms-woocommerce-gift-cards' ),
            'custom_attributes' => array (
                'min'      => 0,
                'step'     => 1,
                'required' => 'required',
            ),
            'default'           => 1,
        ),
        'smgc_shop_logo_url'         => array (
            'name' => __ ( 'Shop logo', 'smms-woocommerce-gift-cards' ),
            'type' => 'smgc_upload_image',
            'id'   => 'smgc_shop_logo_url',
            'desc' => __ ( 'Set the logo of the shop you want to show in the gift card sent to customers. The logo will be showed with a maximum size of 100x60 pixels.', 'smms-woocommerce-gift-cards' ),
        ),
        'smgc_shop_logo_on_gift_card' => array(
            'name' => __('Shop logo in gift card', 'smms-woocommerce-gift-cards'),
            'type' => 'checkbox',
            'id' => 'smgc_shop_logo_on_gift_card',
            'desc' => __('Set if the shop logo should be shown on the gift card template. Disable it if for example, your gift cards template image contains your shop logo', 'smms-woocommerce-gift-cards'),
            'default' => 'yes',
        ),
        'smms_gift_card_header_url'  => array (
            'name' => __ ( 'Logo of the gift card product', 'smms-woocommerce-gift-cards' ),
            'type' => 'smgc_upload_image',
            'id'   => 'smgc_gift_card_header_url',
            'desc' => __ ( 'Select the logo of a default gift card product', 'smms-woocommerce-gift-cards' ),
        ),
        'smgc_template_style'  => array (
            'name' => __ ( 'Template style', 'smms-woocommerce-gift-cards' ),
            'type' => 'radio',
            'options' => array (
                'style1' => __ ( 'Style 1', 'smms-woocommerce-gift-cards' ),
                'style2' => __ ( 'Style 2', 'smms-woocommerce-gift-cards' ),
            ),
            'default' => 'style1',
            'id'   => 'smgc_template_style',
            'desc' => __ ( 'Select the style of the gift card template', 'smms-woocommerce-gift-cards' ),
        ),
        array (
            'type' => 'sectionend',
        ),
    ),
);


return $template_options;


