<?php
if ( ! defined ( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

require_once( SMMS_SMGC_DIR . 'lib/class-smms-woocommerce-gift-cards.php' );
require_once( SMMS_SMGC_DIR . 'lib/class-smms-woocommerce-gift-cards-premium.php' );
require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-backend.php' );
require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-backend-premium.php' );
require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-frontend.php' );
require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-frontend-premium.php' );

require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-gift-card.php' );
require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-gift-card-premium.php' );
require_once( SMMS_SMGC_DIR . 'lib/class-smgc-plugin-fw-loader.php' );

/** Define constant values */
defined( 'SMGC_CUSTOM_POST_TYPE_NAME' ) || define( 'SMGC_CUSTOM_POST_TYPE_NAME', 'gift_card' );
defined( 'SMGC_PHYSICAL_PLACEHOLDER' ) || define( 'SMGC_PHYSICAL_PLACEHOLDER', 'XXXX-XXXX-XXXX-XXXX' );
defined( 'SMGC_GIFT_CARD_PRODUCT_TYPE' ) || define( 'SMGC_GIFT_CARD_PRODUCT_TYPE', 'gift-card' );
defined( 'SMGC_GIFT_CARD_LAST_VIEWED_ID' ) || define( 'SMGC_GIFT_CARD_LAST_VIEWED_ID', 'smgc_last_viewed' );
defined( 'SMGC_AMOUNTS' ) || define( 'SMGC_AMOUNTS', '_gift_card_amounts' );
defined( 'SMGC_PRODUCT_PLACEHOLDER' ) || define( 'SMGC_PRODUCT_PLACEHOLDER', '_smgc_placeholder' );
defined( 'SMGC_MANUAL_AMOUNT_MODE' ) || define( 'SMGC_MANUAL_AMOUNT_MODE', '_smgc_manual_amount_mode' );
defined( 'SMGC_PHYSICAL_GIFT_CARD' ) || define( 'SMGC_PHYSICAL_GIFT_CARD', '_smgc_physical_gift_card' );
defined( 'SMGC_DB_VERSION_OPTION' ) || define( 'SMGC_DB_VERSION_OPTION', 'smms_gift_cards_db_version' );
defined( 'SMGC_CATEGORY_TAXONOMY' ) || define( 'SMGC_CATEGORY_TAXONOMY', 'giftcard-category' );
defined( 'SMGC_PRODUCT_IMAGE' ) || define( 'SMGC_PRODUCT_IMAGE', '_smgc_product_image' );
defined( 'SMGC_PRODUCT_TEMPLATE_DESIGN' ) || define( 'SMGC_PRODUCT_TEMPLATE_DESIGN', '_smgc_show_product_template_design' );

/*  plugin actions */
defined( 'SMGC_ACTION_RETRY_SENDING' ) || define( 'SMGC_ACTION_RETRY_SENDING', 'retry-sending' );
defined( 'SMGC_ACTION_ENABLE_CARD' ) || define( 'SMGC_ACTION_ENABLE_CARD', 'enable-gift-card' );
defined( 'SMGC_ACTION_DISABLE_CARD' ) || define( 'SMGC_ACTION_DISABLE_CARD', 'disable-gift-card' );
defined( 'SMGC_ACTION_ADD_DISCOUNT_TO_CART' ) || define( 'SMGC_ACTION_ADD_DISCOUNT_TO_CART', 'add-discount' );
defined( 'SMGC_ACTION_VERIFY_CODE' ) || define( 'SMGC_ACTION_VERIFY_CODE', 'verify-code' );

/*  gift card post_metas */
defined( 'SMGC_META_GIFT_CARD_AMOUNT' ) || define( 'SMGC_META_GIFT_CARD_AMOUNT', '_smgc_amount' );
defined( 'SMGC_META_GIFT_CARD_AMOUNT_TAX' ) || define( 'SMGC_META_GIFT_CARD_AMOUNT_TAX', '_smgc_amount_tax' );
defined( 'SMGC_META_GIFT_CARD_AMOUNT_BALANCE' ) || define( 'SMGC_META_GIFT_CARD_AMOUNT_BALANCE', '_smgc_amount_balance' );
defined( 'SMGC_META_GIFT_CARD_AMOUNT_BALANCE_TAX' ) || define( 'SMGC_META_GIFT_CARD_AMOUNT_BALANCE_TAX', '_smgc_amount_balance_tax' );
defined( 'SMGC_META_GIFT_CARD_SENT' ) || define( 'SMGC_META_GIFT_CARD_SENT', '_smgc_email_sent' );
defined( 'SMGC_META_GIFT_CARD_ORDER_ID' ) || define( 'SMGC_META_GIFT_CARD_ORDER_ID', '_smgc_order_id' );
defined( 'SMGC_META_GIFT_CARD_ORDERS' ) || define( 'SMGC_META_GIFT_CARD_ORDERS', '_smgc_orders' );
defined( 'SMGC_META_GIFT_CARD_CUSTOMER_USER' ) || define( 'SMGC_META_GIFT_CARD_CUSTOMER_USER', '_smgc_customer_user' );
defined( 'SMGC_ORDER_ITEM_DATA' ) || define( 'SMGC_ORDER_ITEM_DATA', '_smgc_order_item_data' );

/*  order item metas    */
defined( 'SMGC_META_GIFT_CARD_POST_ID' ) || define( 'SMGC_META_GIFT_CARD_POST_ID', '_smgc_gift_card_post_id' );
defined( 'SMGC_META_GIFT_CARD_DELIVERY_DATE' ) || define( 'SMGC_META_GIFT_CARD_DELIVERY_DATE', '_smgc_delivery_date' );
defined( 'SMGC_META_GIFT_CARD_STATUS' ) || define( 'SMGC_META_GIFT_CARD_STATUS', '_smgc_gift_card_status' );

/* Gift card status */
defined( 'GIFT_CARD_STATUS_DISABLED' ) || define( 'GIFT_CARD_STATUS_DISABLED', 'smgc-disabled' );
defined( 'GIFT_CARD_STATUS_DISMISSED' ) || define( 'GIFT_CARD_STATUS_DISMISSED', 'smgc-dismissed' );
defined( 'GIFT_CARD_STATUS_ENABLED' ) || define( 'GIFT_CARD_STATUS_ENABLED', 'publish' );
defined( 'GIFT_CARD_STATUS_PRE_PRINTED' ) || define( 'GIFT_CARD_STATUS_PRE_PRINTED', 'smgc-pre-printed' );

/* Gift card table columns */
defined( 'SMGC_TABLE_COLUMN_ORDER' ) || define( 'SMGC_TABLE_COLUMN_ORDER', 'purchase_order' );
defined( 'SMGC_TABLE_COLUMN_INFORMATION' ) || define( 'SMGC_TABLE_COLUMN_INFORMATION', 'information' );
defined( 'SMGC_TABLE_COLUMN_AMOUNT' ) || define( 'SMGC_TABLE_COLUMN_AMOUNT', 'amount' );
defined( 'SMGC_TABLE_COLUMN_BALANCE' ) || define( 'SMGC_TABLE_COLUMN_BALANCE', 'balance' );
defined( 'SMGC_TABLE_COLUMN_DEST_ORDERS' ) || define( 'SMGC_TABLE_COLUMN_DEST_ORDERS', 'dest_orders' );
defined( 'SMGC_TABLE_COLUMN_DEST_ORDERS_TOTAL' ) || define( 'SMGC_TABLE_COLUMN_DEST_ORDERS_TOTAL', 'dest_order_total' );
defined( 'SMGC_TABLE_COLUMN_ACTIONS' ) || define( 'SMGC_TABLE_COLUMN_ACTIONS', 'gift_card_actions' );


if ( ! function_exists( 'smgc_required' ) ) {
	/**
	 * Give the "required" attribute, if in use
	 *
	 * @param           $value
	 * @param bool|true $compare
	 * @param bool|true $echo
	 *
	 * @return string
	 * @author Lorenzo Giuffrida
	 * @since  1.0.0
	 */
	function smgc_required( $value, $compare = true, $echo = true ) {
		$required = ( $value === $compare ) || ( $value instanceof $compare );
		$result   = $required ? "required" : '';
		if ( $echo ) {
			echo $result;
		} else {
			return $result;
		}
	}
}

if ( ! function_exists( 'smgc_can_create_gift_card' ) ) {
	/**
	 * Verify if current user can create product of type gift card
	 */
	function smgc_can_create_gift_card() {
		return apply_filters( 'smgc_can_create_gift_card', true );
	}
}

if ( ! function_exists( 'smgc_get_status_label' ) ) {
	/**
	 * Retrieve the status label for every gift card status
	 *
	 * @param SMMS_SMGC_Gift_Card $gift_card
	 *
	 * @return string
	 */
	function smgc_get_status_label( $gift_card ) {
		$label = '';

		switch ( $gift_card->status ) {
			case GIFT_CARD_STATUS_DISABLED:
				$label = __( "The gift card has been disabled", 'smms-woocommerce-gift-cards' );
				break;
			case GIFT_CARD_STATUS_ENABLED:
				$label = __( "Valid", 'smms-woocommerce-gift-cards' );
				break;
			case GIFT_CARD_STATUS_DISMISSED:
				$label = __( "No longer valid, replaced by another code", 'smms-woocommerce-gift-cards' );
				break;
		}

		return $label;
	}
}

if ( ! function_exists( 'smgc_get_order_item_giftcards' ) ) {
	/**
	 * Retrieve the gift card ids associated to an order item
	 *
	 * @param int $order_item_id
	 *
	 * @return string|void
	 * @author Lorenzo Giuffrida
	 * @since  1.0.0
	 */
	function smgc_get_order_item_giftcards( $order_item_id ) {

		/*
		 * Let third party plugin to change the $order_item_id
		 * 
		 * @since 1.3.7
		 */
		$order_item_id = apply_filters( 'smms_get_order_item_gift_cards', $order_item_id );
		$gift_ids      = wc_get_order_item_meta( $order_item_id, SMGC_META_GIFT_CARD_POST_ID );

		if ( is_numeric( $gift_ids ) ) {
			$gift_ids = array( $gift_ids );
		}

		if ( ! is_array( $gift_ids ) ) {
			$gift_ids = array();
		}

		return $gift_ids;
	}
}

if ( ! function_exists( 'smgc_set_order_item_giftcards' ) ) {
	/**
	 * Retrieve the gift card ids associated to an order item
	 *
	 * @param int   $order_item_id the order item
	 * @param array $ids           the array of gift card ids associated to the order item
	 *
	 * @return string|void
	 * @author Lorenzo Giuffrida
	 * @since  1.0.0
	 */
	function smgc_set_order_item_giftcards( $order_item_id, $ids ) {

		$ids = apply_filters( 'smms_smgc_set_order_item_meta_gift_card_ids', $ids, $order_item_id );

		wc_update_order_item_meta( $order_item_id, SMGC_META_GIFT_CARD_POST_ID, $ids );

		do_action( 'smms_smgc_set_order_item_meta_gift_card_ids_updated', $order_item_id, $ids );
	}
}

if ( ! function_exists( 'wc_help_tip' ) && function_exists( 'WC' ) && version_compare( WC()->version, '2.5.0', '<' ) ) {

	/**
	 * Display a WooCommerce help tip. (Added for compatibility with WC 2.4)
	 *
	 * @since  2.5.0
	 *
	 * @param  string $tip        Help tip text
	 * @param  bool   $allow_html Allow sanitized HTML if true or escape
	 *
	 * @return string
	 */
	function wc_help_tip( $tip, $allow_html = false ) {
		if ( $allow_html ) {
			$tip = wc_sanitize_tooltip( $tip );
		} else {
			$tip = esc_attr( $tip );
		}

		return '<span class="woocommerce-help-tip" data-tip="' . $tip . '"></span>';
	}

}


if ( ! function_exists( 'smms_get_attachment_image_url' ) ) {

	/**
	 * @return string
	 */
	function smms_get_attachment_image_url( $attachment_id, $size = 'thumbnail' ) {

		if ( function_exists( 'wp_get_attachment_image_url' ) ) {
			$header_image_url = wp_get_attachment_image_url( $attachment_id, $size );
		} else {
			$header_image     = wp_get_attachment_image_src( $attachment_id, $size );
			$header_image_url = $header_image['url'];
		}

		return $header_image_url;
	}

}