<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_Product_Gift_Card' ) ) {

	/**
	 *
	 * @class   SMMS_SMGC_Gift_Card
	 *
	 * @since   1.0.0
	 * @author  Lorenzo Giuffrida
	 */
	class WC_Product_Gift_Card extends WC_Product {

		/**
		 * @var array the amounts set for the product
		 */
		public $amounts;

		/**
		 * Initialize a gift card product.
		 *
		 * @param mixed $product
		 */
		public function __construct( $product ) {
			$this->product_type = SMGC_GIFT_CARD_PRODUCT_TYPE;

			parent::__construct( $product );
			$this->amounts = $this->get_amounts();
		}

		/**
		 * A virtual gift card is also downloadable
		 *
		 * @return bool
		 */
		public function is_downloadable() {
			return $this->is_virtual();
		}

		/**
		 * Retrieve the number of current amounts for this product
		 *
		 * @return int
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		private function get_amounts_count() {
			return count( $this->amounts );
		}

		/**
		 * Retrieve a list of amounts for the current product
		 *
		 * @return array
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function get_amounts() {
			$result = array();

			if ( $this->id ) {
				global $sitepress;
				$product_id = $sitepress ? yit_wpml_object_id( $this->id, 'product', true, $sitepress->get_default_language() ) : $this->id;

				$result = get_post_meta( $product_id, SMGC_AMOUNTS, true );
			}

			$result = is_array( $result ) ? $result : array();

			return apply_filters( 'smms_smgc_gift_card_amounts', $result, $this );
		}

		/**
		 * Returns false if the product cannot be bought.
		 *
		 * @return bool
		 */
		public function is_purchasable() {

			$purchasable = $this->get_amounts_count();

			return apply_filters( 'woocommerce_is_purchasable', $purchasable, $this );
		}

		public function update_amounts( $amounts = array() ) {
			update_post_meta( $this->id, SMGC_AMOUNTS, $amounts );
		}

		public function update_design_status( $status ) {
			update_post_meta( $this->id, SMGC_PRODUCT_TEMPLATE_DESIGN, $status );
		}

		public function get_design_status() {
			$id = $this->id;
			if ( $this->id ) {
				global $sitepress;
				$id = $sitepress ? yit_wpml_object_id( $this->id, 'product', true, $sitepress->get_default_language() ) : $this->id;
			}

			return get_post_meta( $id, SMGC_PRODUCT_TEMPLATE_DESIGN, true );
		}

		/**
		 * Update the manual amount status.
		 * Available values are "global", "accept" and "reject"
		 *
		 * @param string $status
		 */
		public function update_manual_amount_status( $status ) {
			update_post_meta( $this->id, SMGC_MANUAL_AMOUNT_MODE, $status );
		}

		/**
		 * Retrieve the manual amount status for this product.
		 *
		 * Available values are "global", "accept" and "reject"
		 * @return mixed
		 */
		public function get_manual_amount_status() {

			$id = $this->id;
			if ( $this->id ) {
				global $sitepress;
				$id = $sitepress ? yit_wpml_object_id( $this->id, 'product', true, $sitepress->get_default_language() ) : $this->id;
			}

			return get_post_meta( $id, SMGC_MANUAL_AMOUNT_MODE, true );
		}


		/**
		 * Returns the price in html format
		 *
		 * @access public
		 *
		 * @param string $price (default: '')
		 *
		 * @return string
		 */
		public function get_price_html( $price = '' ) {
			$amounts = $this->get_amounts_to_be_shown();

			// No price for current gift card
			if ( ! count( $amounts ) ) {
				$price = apply_filters( 'smms_woocommerce_gift_cards_empty_price_html', '', $this );
			} else {
				ksort( $amounts, SORT_NUMERIC );

				$min_price = current( $amounts );
				$min_price = $min_price['price'];
				$max_price = end( $amounts );
				$max_price = $max_price['price'];

				$price = $min_price !== $max_price ? sprintf( _x( '%1$s&ndash;%2$s', 'Price range: from-to', 'smms-woocommerce-gift-cards' ), wc_price( $min_price ), wc_price( $max_price ) ) : wc_price( $min_price );
				$price = apply_filters( 'smms_woocommerce_gift_cards_amount_range', $price, $this );
			}

			return apply_filters( 'woocommerce_get_price_html', $price, $this );
		}

		/**
		 * Retrieve an array of gift cards amounts with the corrected value to be shown(inclusive or not inclusive taxes)
		 *
		 * @return array
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function get_amounts_to_be_shown() {
			$amounts          = $this->amounts;
			$amounts_to_show  = array();
			$tax_display_mode = get_option( 'woocommerce_tax_display_shop' );

			$original_amounts = $this->get_amounts();
			$index            = 0;

			foreach ( $amounts as $amount ) {
				if ( 'incl' === $tax_display_mode ) {
					$price = $this->get_price_including_tax( 1, $amount );
				} else {
					$price = $this->get_price_excluding_tax( 1, $amount );
				}

				$original_amount                     = $original_amounts[ $index ];
				$amounts_to_show[ $original_amount ] = array(
					'price' => $price,
					'title' => wc_price( $price )
				);
				$index ++;
			}

			return apply_filters( 'smms_smgc_gift_cards_amounts', $amounts_to_show, $this->id );
		}

		/**
		 * Get the add to cart button text
		 *
		 * @return string
		 */
		public function add_to_cart_text() {

			return apply_filters( 'smms_woocommerce_gift_cards_add_to_cart_text', __( 'Select amount', 'smms-woocommerce-gift-cards' ), $this );
		}

		/**
		 * Add a new amount to the gift cards
		 *
		 * @param float $amount
		 *
		 * @return bool
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function add_amount( $amount ) {

			$amounts = $this->get_amounts();

			if ( ! in_array( $amount, $amounts ) ) {

				$amounts[] = $amount;
				sort( $amounts, SORT_NUMERIC );
				$this->save_amounts( $amounts );
			}

			return false;
		}

		/**
		 * Remove an amount from the amounts list
		 *
		 * @param float $amount
		 *
		 * @return bool
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function remove_amount( $amount ) {

			$amounts = $this->get_amounts();

			if ( in_array( $amount, $amounts ) ) {
				if ( ( $key = array_search( $amount, $amounts ) ) !== false ) {
					unset( $amounts[ $key ] );
				}

				$this->save_amounts( $amounts );

				return true;
			}

			return false;
		}


		/**
		 * Save the gift card product amounts
		 *
		 * @param array $amounts current amount list
		 *
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		private function save_amounts( $amounts = array() ) {
			update_post_meta( $this->id, SMGC_AMOUNTS, $amounts );
		}

		/**
		 * Retrieve the custom image set from the edit product page for a specific gift card product
		 *
		 * @param string $size
		 *
		 * @return mixed
		 */
		public function get_manual_header_image( $size = 'full' ) {
			$image_url = '';
			$id = $this->id;

			if ( $this->id ) {
				global $sitepress;
				$id = $sitepress ? yit_wpml_object_id( $this->id, 'product', true, $sitepress->get_default_language() ) : $this->id;
			}

			$image_id = get_post_meta( $id, SMGC_PRODUCT_IMAGE, true );
			if ( $image_id ) {
				$image     = wp_get_attachment_image_src( $image_id, $size );
				$image_url = $image[0];
			}

			return $image_url;
		}

		/**
		 * Set the header image for a gift card product
		 *
		 * @param int $attachment_id
		 */
		public function set_header_image( $attachment_id ) {

			update_post_meta( $this->id, SMGC_PRODUCT_IMAGE, $attachment_id );
		}

		/**
		 * Unset the header image for a gift card product
		 *
		 */
		public function unset_header_image() {

			delete_post_meta( $this->id, SMGC_PRODUCT_IMAGE );
		}
	}
}