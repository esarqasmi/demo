<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( "WC_Email" ) ) {
	require_once( WC()->plugin_path() . '/includes/emails/class-wc-email.php' );
}

if ( ! class_exists( "SMMS_SMGC_Email_Send_Gift_Card" ) ) {
	/**
	 * Create and send a digital gift card to the specific recipient
	 *
	 * @since 0.1
	 * @extends \WC_Email
	 */
	class SMMS_SMGC_Email_Send_Gift_Card extends WC_Email {
		/**
		 * An introductional message from the shop owner
		 */
		public $introductory_text;

		/**
		 * Set email defaults
		 *
		 * @since 0.1
		 */
		public function __construct() {
			// set ID, this simply needs to be a unique name
			$this->id = 'smgc-email-send-gift-card';

			// this is the title in WooCommerce Email settings
			$this->title = __( "SMMS Gift Cards - Dispatch of the coupon", 'smms-woocommerce-gift-cards' );

			// this is the description in WooCommerce email settings
			$this->description = __( 'Send the digital gift card to the email address selected during the purchase', 'smms-woocommerce-gift-cards' );

			// these are the default heading and subject lines that can be overridden using the settings
			$this->heading = __( 'Your gift card', 'smms-woocommerce-gift-cards' );
			$this->subject = __( '[{site_title}] You have received a gift card', 'smms-woocommerce-gift-cards' );

			// these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
			$this->template_html  = 'emails/send-gift-card.php';
			$this->template_plain = 'emails/plain/send-gift-card.php';

			$this->introductory_text = __( 'Hi {recipient_name}, you have received this gift card from {sender}, use it on our online shop.', 'smms-woocommerce-gift-cards' );

			// Trigger on specific action call
			add_action( 'smgc-email-send-gift-card_notification',
				array(
					$this,
					'trigger'
				) );

			parent::__construct();
			$this->email_type = "html";
		}

		/**
		 * Send the digital gift card to the recipient
		 *
		 * @param int|SMGC_Gift_Card_Premium|SMMS_SMGC_Gift_Card $object it's the order id or the gift card id or the gift card instance to be sent
		 *
		 * @return bool|void
		 */
		public function trigger( $object ) {

			if ( ! ( $object instanceof SMGC_Gift_Card_Premium ) ) {
				return false;
			}

			if ( ! $object->exists() ) {
				return false;
			}

			$this->object    = $object;
			$this->recipient = $object->recipient;

			$this->introductory_text = $this->get_option( 'introductory_text', __( 'Hi {recipient_name}, you have received this gift card from {sender}, use it on our online shop.', 'smms-woocommerce-gift-cards' ) );
			$recipient_name          = $this->object->recipient_name ? $this->object->recipient_name : '';
			$sender_name             = $this->object->sender_name ? $this->object->sender_name : __( 'a friend', 'smms-woocommerce-gift-cards' );

			$this->introductory_text = str_replace(
				array(
					"{sender}",
					"{recipient_name}"
				),
				array(
					$sender_name,
					$recipient_name
				),
				$this->introductory_text
			);

			$result = $this->send( $this->get_recipient(),
				$this->get_subject(),
				$this->get_content(),
				$this->get_headers(),
				$this->get_attachments() );

			if ( $result ) {
				//  Set the gift card as sent
				$object->set_as_sent();
			}

			return $result;
		}

		/**
		 * get_content_html function.
		 *
		 * @since 0.1
		 * @return string
		 */
		public function get_content_html() {
			ob_start();
			wc_get_template( $this->template_html, array(
				'gift_card'         => $this->object,
				'introductory_text' => $this->introductory_text,
				'email_heading'     => $this->get_heading(),
				'email_type'        => $this->email_type,
				'sent_to_admin'     => false,
				'plain_text'        => false,
				'email'             => $this,
			),
				'',
				SMMS_SMGC_TEMPLATES_DIR );

			return ob_get_clean();
		}


		/**
		 * Initialize Settings Form Fields
		 *
		 * @since 0.1
		 */
		public function init_form_fields() {
			$this->form_fields = array(
				'enabled'           => array(
					'title'   => __( 'Enable/Disable', 'woocommerce' ),
					'type'    => 'checkbox',
					'label'   => __( 'Enable this email notification', 'woocommerce' ),
					'default' => 'yes',
				),
				'subject'           => array(
					'title'       => __( 'Subject', 'woocommerce' ),
					'type'        => 'text',
					'description' => sprintf( __( 'Defaults to <code>%s</code>', 'woocommerce' ), $this->subject ),
					'placeholder' => '',
					'default'     => '',
				),
				'heading'           => array(
					'title'       => __( 'Email Heading', 'woocommerce' ),
					'type'        => 'text',
					'description' => sprintf( __( 'Defaults to <code>%s</code>', 'woocommerce' ), $this->heading ),
					'placeholder' => '',
					'default'     => '',
				),
				'introductory_text' => array(
					'title'       => __( 'Introductive message', 'smms-woocommerce-gift-cards' ),
					'type'        => 'textarea',
					'description' => sprintf( __( 'Defaults to <code>%s</code>', 'woocommerce' ), $this->introductory_text ),
					'placeholder' => '',
					'default'     => '',
				),
			);
		}
	} // end \SMMS_SMGC_Email_Send_Gift_Card class
}

return new SMMS_SMGC_Email_Send_Gift_Card();