<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( ! class_exists( 'SMMS_SMGC_Emails' ) ) {

	/**
	 *
	 * @class   SMMS_SMGC_Emails
	 *
	 * @since   1.0.0
	 * @author  Lorenzo Giuffrida
	 */
	class SMMS_SMGC_Emails {
		/**
		 * Single instance of the class
		 *
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize plugin and registers actions and filters to be used
		 *
		 * @since  1.0
		 * @author Lorenzo Giuffrida
		 */
		protected function __construct() {

			/**
			 * Add an email action for sending the digital gift card
			 */
			add_filter( 'woocommerce_email_actions', array( $this, 'add_gift_cards_trigger_action' ) );

			/**
			 * Locate the plugin email templates
			 */
			add_filter( 'woocommerce_locate_core_template', array( $this, 'locate_core_template' ), 10, 3 );

			/**
			 * Add the email used to send digital gift card to woocommerce email tab
			 */
			add_filter( 'woocommerce_email_classes', array( $this, 'add_woocommerce_email_classes' ) );

			/**
			 * Add entry on resend order email list
			 */
			add_filter( 'woocommerce_resend_order_emails_available', array( $this, 'resend_gift_card_code' ) );

			/**
			 * Add information to the email footer
			 */
			add_action( 'woocommerce_email_footer', array(
				$this,
				'add_footer_information'
			) );

			/**
			 * Add CSS style to gift card emails header
			 */
			add_action( 'woocommerce_email_header', array(
				$this,
				'include_css_for_emails'
			), 10, 2 );

			/**
			 * Show an introductory text before the gift cards editor
			 */
			add_action( 'smgc_gift_cards_email_before_preview', array(
				$this,
				'show_introductory_text'
			), 10, 2 );

			/**
			 * Show the link for cart discount on the gift card email
			 */
			add_action( 'smgc_gift_card_email_after_preview', array(
				$this,
				'show_gift_card_expiration'
			), 9 );

			/**
			 * Show the link for cart discount on the gift card email
			 */
			add_action( 'smgc_gift_card_email_after_preview', array(
				$this,
				'show_link_for_cart_discount'
			), 10 );

			/**
			 * Show the product suggestion on the gift card email
			 */
			add_action( 'smgc_gift_card_email_after_preview', array(
				$this,
				'show_product_suggestion'
			), 15 );

			/**
			 * We'll add additional information and link near the product name, but we need to show it
			 * only on view order page, while the same action (woocommerce_order_item_meta_start) is used for emails where we do not want
			 * to show it.
			 * Hack it enabling and disabling the feature using the action/filter available at the moment
			 *
			 * @since WooCommerce 2.5.5
			 *
			 * Remove some action/filter that cause unwanted data to be shown on emails
			 */
			add_filter( 'woocommerce_order_item_quantity_html', array( $this, 'enable_edit_hooks_for_emails' ) );

			/**
			 * Add the previously removed action/filter that cause unwanted data to be shown on emails
			 */
			add_action( 'woocommerce_order_item_meta_end', array( $this, 'disable_edit_hooks_for_emails' ) );

			add_action( 'smgc_start_gift_cards_sending', array(
				$this,
				'send_delayed_gift_cards'
			) );

			add_action( 'smms_smgc_send_gift_card_email', array(
				$this,
				'send_gift_card_email'
			) );
		}

		public function get_postdated_gift_cards( $send_date ) {
			$args = array(
				'meta_query' => array(
					array(
						'key'     => SMGC_META_GIFT_CARD_DELIVERY_DATE,
						'value'   => $send_date,
						'compare' => '<=',
					),
					array(
						'key'     => SMGC_META_GIFT_CARD_SENT,
						'value'   => '1',
						'compare' => 'NOT EXISTS',
					),
				),

				'post_type'      => SMGC_CUSTOM_POST_TYPE_NAME,
				'fields'         => 'ids',
				'post_status'    => 'publish',
				'posts_per_page' => - 1,
			);

			$ids = get_posts( $args );

			return $ids;
		}

		/**
		 * Send the digital gift cards that should be received on specific date.
		 *
		 * @param string $send_date
		 */
		public function send_delayed_gift_cards( $send_date = null ) {

			if ( ! class_exists( "SMMS_SMGC_Email_Send_Gift_Card" ) ) {
				include( 'emails/class-smms-smgc-email-send-gift-card.php' );
			}

			if ( null == $send_date ) {
				$send_date = current_time( 'Y-m-d', 0 );
			}

			// retrieve gift card to be sent for specific date
			$gift_cards_ids = $this->get_postdated_gift_cards( $send_date );

			foreach ( $gift_cards_ids as $gift_card_id ) {
				// send digital single gift card to recipient
				$gift_card = new SMGC_Gift_Card_Premium( array( 'ID' => $gift_card_id ) );

				if ( ! $gift_card->exists() ) {
					continue;
				}

				if ( ! $gift_card->is_virtual() || empty( $gift_card->recipient ) ) {
					// not a digital gift card or missing recipient
					continue;
				}

//				if ( $gift_card->has_been_sent() ) {
//					//  avoid sending emails more than one time
//					continue;
//				}

				$this->send_gift_card_email( $gift_card );
			}
		}

		/**
		 * send the gift card code email
		 *
		 * @param SMMS_SMGC_Gift_Card_Premium|int $gift_card the gift card
		 *
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function send_gift_card_email( $gift_card ) {

			if ( is_numeric( $gift_card ) ) {
				$gift_card = new SMGC_Gift_Card_Premium( array( 'ID' => $gift_card ) );
			}

			if ( ! $gift_card->exists() ) {
				//  it isn't a gift card
				return;
			}

			if ( ! $gift_card->is_virtual() || empty( $gift_card->recipient ) ) {
				// not a digital gift card or missing recipient
				return;
			}

			WC()->mailer();
			do_action( 'smgc-email-send-gift-card_notification', $gift_card );

			do_action( 'smms_smgc_gift_card_email_sent', $gift_card );
		}

		/**
		 * Show a message with the gift card expiration date
		 *
		 * @param $gift_card
		 */
		public function show_gift_card_expiration( $gift_card ) {
			if ( $gift_card->expiration ) {
				$message = apply_filters( 'smms_smgc_gift_card_email_expiration_message',
					sprintf( _x( 'This gift card code will be valid until %s (Y-m-d)', 'gift card expiration date', 'smms-woocommerce-gift-cards' ), date( 'Y-m-d', $gift_card->expiration ) ) );

				echo '<div class="smgc-expiration-message">' . $message . '</div>';
			}
		}

		/**
		 * Show a link that let the customer to go to the website, adding the discount to the cart
		 *
		 * @param SMGC_Gift_Card_Premium $gift_card
		 *
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function show_link_for_cart_discount( $gift_card ) {

			if ( ! SMMS_SMGC()->automatic_discount ) {
				return;
			}

			$shop_page_url = ( - 1 != wc_get_page_id( 'shop' ) ) ? get_permalink( wc_get_page_id( 'shop' ) ) : site_url();

			$args = array(
				SMGC_ACTION_ADD_DISCOUNT_TO_CART => $gift_card->gift_card_number,
				SMGC_ACTION_VERIFY_CODE          => SMMS_SMGC()->hash_gift_card( $gift_card ),
			);

			$apply_discount_url = esc_url( add_query_arg( $args, $shop_page_url ) );

			wc_get_template( 'emails/automatic-discount.php',
				array(
					'apply_discount_url' => apply_filters( 'smms_smgc_email_automatic_cart_discount_url', $apply_discount_url ),
				),
				'',
				SMMS_SMGC_TEMPLATES_DIR );
		}

		/**
		 * Remove some action/filter that cause unwanted data to be shown on emails
		 */
		public function disable_edit_hooks_for_emails() {
			remove_action( 'woocommerce_order_item_meta_start', array(
				SMMS_SMGC()->frontend,
				'edit_gift_card',
			), 10 );
		}

		/**
		 * Add the previously removed action/filter that cause unwanted data to be shown on emails
		 *
		 * @param string $title the text being shown
		 *
		 * @return string
		 */
		public function enable_edit_hooks_for_emails( $title ) {
			add_action( 'woocommerce_order_item_meta_start', array(
				SMMS_SMGC()->frontend,
				'edit_gift_card',
			), 10, 3 );

			return $title;
		}

		/**
		 * Show the product suggestion associated to the gift card
		 *
		 * @param SMGC_Gift_Card_Premium $gift_card
		 *
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function show_product_suggestion( $gift_card ) {

			if ( ! $gift_card->product_as_present ) {
				return;
			}

			//  The customer has suggested a product when he bought the gift card
			if ( $gift_card->present_variation_id ) {
				$product = wc_get_product( $gift_card->present_variation_id );
			} else {
				$product = wc_get_product( $gift_card->present_product_id );
			}

			wc_get_template( 'emails/product-suggestion.php',
				array(
					'gift_card' => $gift_card,
					'product'   => $product,
				),
				'',
				SMMS_SMGC_TEMPLATES_DIR );
		}

		/**
		 * Show the introductory message on the email being sent
		 *
		 * @param string              $text
		 * @param SMMS_SMGC_Gift_Card $gift_card
		 *
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function show_introductory_text( $text, $gift_card ) {
			?>
			<p class="center-email"><?php echo apply_filters( 'smgc_gift_cards_email_before_preview_text', $text, $gift_card ); ?></p>
			<?php
		}


		/**
		 * Add CSS style to gift card emails header
		 */
		public function include_css_for_emails( $email_heading, $email = null ) {
			if ( $email == null ) {
				return;
			}

			if ( ! isset( $email->object ) ) {
				return;
			}

			if ( ! $email->object instanceof SMMS_SMGC_Gift_Card ) {
				return;
			}

			echo '<style type="text/css">';

			include( SMMS_SMGC_ASSETS_DIR . "/css/smgc-frontend.css" );

			wc_get_template( 'emails/style.css',
				'',
				'',
				SMMS_SMGC_TEMPLATES_DIR );

			echo '</style>';
		}

		/**
		 * Add gift card email to the available email on resend order email feature
		 *
		 * @param array $emails current emails
		 *
		 * @return array
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function resend_gift_card_code( $emails ) {
			$emails[] = 'smgc-email-send-gift-card';

			return $emails;
		}


		/**
		 * Append CSS for the email being sent to the customer
		 *
		 * @param WC_Email $email the email content
		 */
		public function add_footer_information( $email = null ) {
			if ( $email == null ) {
				return;
			}

			if ( ! isset( $email->object ) ) {
				return;
			}

			if ( ! $email->object instanceof SMMS_SMGC_Gift_Card ) {
				return;
			}

			wc_get_template( 'emails/gift-card-footer.php',
				array(
					'email'     => $email,
					'shop_name' => SMMS_SMGC()->shop_name,
				),
				'',
				SMMS_SMGC_TEMPLATES_DIR );
		}

		/**
		 * Add an email action for sending the digital gift card
		 *
		 * @param array $actions list of current actions
		 *
		 * @return array
		 */
		function add_gift_cards_trigger_action( $actions ) {
			//  Add trigger action for sending digital gift card
			$actions[] = 'smgc-email-send-gift-card';
			$actions[] = 'smgc-email-notify-customer';

			return $actions;
		}

		/**
		 * Locate the plugin email templates
		 *
		 * @param $core_file
		 * @param $template
		 * @param $template_base
		 *
		 * @return string
		 */
		public function locate_core_template( $core_file, $template, $template_base ) {
			$custom_template = array(
				'emails/send-gift-card.php',
				'emails/plain/send-gift-card.php',
				'emails/notify-customer.php',
				'emails/plain/notify-customer.php',
			);

			if ( in_array( $template, $custom_template ) ) {
				$core_file = SMMS_SMGC_TEMPLATES_DIR . $template;
			}

			return $core_file;
		}


		/**
		 * Add the email used to send digital gift card to woocommerce email tab
		 *
		 * @param string $email_classes current email classes
		 *
		 * @return mixed
		 */
		public function add_woocommerce_email_classes( $email_classes ) {
			// add the email class to the list of email classes that WooCommerce loads
			$email_classes['smgc-email-send-gift-card']  = include( 'emails/class-smms-smgc-email-send-gift-card.php' );
			$email_classes['smgc-email-notify-customer'] = include( 'emails/class-smms-smgc-email-notify-customer.php' );

			return $email_classes;
		}


	}
}

SMMS_SMGC_Emails::get_instance();