<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( ! class_exists( 'SMMS_SMGC_Gift_Cards_Table' ) ) {

	/**
	 *
	 * @class   SMMS_SMGC_Gift_Cards_Table
	 *
	 * @since   1.0.0
	 * @author  Lorenzo Giuffrida
	 */
	class SMMS_SMGC_Gift_Cards_Table {
		/**
		 * Single instance of the class
		 *
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize plugin and registers actions and filters to be used
		 *
		 * @since  1.0
		 * @author Lorenzo Giuffrida
		 */
		protected function __construct() {

			// Add to admin_init function
			add_filter( 'manage_edit-gift_card_columns', array( $this, 'add_custom_columns_title' ) );

			// Add to admin_init function
			add_action( 'manage_gift_card_posts_custom_column', array(
				$this,
				'add_custom_columns_content',
			), 10, 2 );
		}

		/**
		 * Add custom columns to custom post type table
		 *
		 * @param array $defaults current columns
		 *
		 * @return array new columns
		 */
		function add_custom_columns_title( $defaults ) {
			$columns = array_slice( $defaults, 0, 2 );

			$columns[ SMGC_TABLE_COLUMN_ORDER ]             = __( "Order", 'smms-woocommerce-gift-cards' );
			$columns[ SMGC_TABLE_COLUMN_AMOUNT ]            = __( "Amount", 'smms-woocommerce-gift-cards' );
			$columns[ SMGC_TABLE_COLUMN_BALANCE ]           = __( "Balance", 'smms-woocommerce-gift-cards' );
			$columns[ SMGC_TABLE_COLUMN_DEST_ORDERS ]       = __( "Orders", 'smms-woocommerce-gift-cards' );
			$columns[ SMGC_TABLE_COLUMN_DEST_ORDERS_TOTAL ] = __( "Order total", 'smms-woocommerce-gift-cards' );
			$columns[ SMGC_TABLE_COLUMN_INFORMATION ]       = __( "Information", 'smms-woocommerce-gift-cards' );
			$columns[ SMGC_TABLE_COLUMN_ACTIONS ]           = '';

			return array_merge( $columns, array_slice( $defaults, 1 ) );
		}

		/**
		 * @param WC_Order|int $order
		 *
		 * @return int
		 */
		private function get_order_number_and_details( $order ) {

			if ( is_numeric( $order ) ) {
				$order = wc_get_order( $order );
			}

			if ( ! $order instanceof WC_Order ) {
				return '';
			}
			$order_id = $order->id;

			if ( $order->user_id ) {
				$user_info = get_userdata( $order->user_id );
			}

			if ( ! empty( $user_info ) ) {
				$username = '<a href="user-edit.php?user_id=' . absint( $user_info->ID ) . '">';

				if ( $user_info->first_name || $user_info->last_name ) {
					$username .= esc_html( ucfirst( $user_info->first_name ) . ' ' . ucfirst( $user_info->last_name ) );
				} else {
					$username .= esc_html( ucfirst( $user_info->display_name ) );
				}

				$username .= '</a>';

			} else {

				if ( $order->billing_first_name || $order->billing_last_name ) {
					$username = trim( $order->billing_first_name . ' ' . $order->billing_last_name );
				} else {
					$username = __( 'Guest', 'smms-woocommerce-gift-cards' );
				}
			}

			return sprintf( _x( '%s by %s', 'Order number by X', 'smms-woocommerce-gift-cards' ),
				'<a href="' . admin_url( 'post.php?post=' . absint( $order_id ) . '&action=edit' ) . '" class="row-title"><strong>#' .
				esc_attr( $order->get_order_number() ) . '</strong></a>',
				$username );
		}


		/**
		 * show content for custom columns
		 *
		 * @param $column_name string column shown
		 * @param $post_ID     int     post to use
		 */
		function add_custom_columns_content( $column_name, $post_ID ) {

			$gift_card = new SMGC_Gift_Card_Premium( array( 'ID' => $post_ID ) );

			if ( ! $gift_card->exists() ) {
				return;
			}

			switch ( $column_name ) {
				case SMGC_TABLE_COLUMN_ORDER :

					if ( $gift_card->order_id ) {
						echo $this->get_order_number_and_details( $gift_card->order_id );
					} else {
						_e( 'Created manually', 'smms-woocommerce-gift-cards' );
					}

					break;

				case SMGC_TABLE_COLUMN_AMOUNT :

					$_amount     = empty( $gift_card->amount ) ? 0.00 : $gift_card->amount;
					$_amount_tax = empty( $gift_card->amount_tax ) ? 0.00 : $gift_card->amount_tax;

					echo wc_price( $_amount + $_amount_tax );

					break;

				case SMGC_TABLE_COLUMN_BALANCE:

					$_amount     = empty( $gift_card->balance ) ? 0.00 : $gift_card->balance;
					$_amount_tax = empty( $gift_card->balance_tax ) ? 0.00 : $gift_card->balance_tax;

					echo wc_price( $_amount + $_amount_tax );

					break;

				case SMGC_TABLE_COLUMN_DEST_ORDERS:
					$orders = $gift_card->get_registered_orders();
					if ( $orders ) {
						foreach ( $orders as $order_id ) {
							echo $this->get_order_number_and_details( $order_id );
							echo "<br>";
						}
					} else {
						_e( "The code has not been used yet", 'smms-woocommerce-gift-cards' );
					}

					break;

				case SMGC_TABLE_COLUMN_INFORMATION:

					$this->show_details_on_gift_cards_table( $post_ID, $gift_card );

					break;

				case SMGC_TABLE_COLUMN_DEST_ORDERS_TOTAL:

					$orders = $gift_card->get_registered_orders();
					$total  = 0.00;

					if ( $orders ) {
						foreach ( $orders as $order_id ) {

							$the_order = wc_get_order( $order_id );
							if ( $the_order ) {
								//  From version 1.2.10, show the order totals instead of subtotals
								//  $order_total = floatval(preg_replace('#[^\d.]#', '', $the_order->get_subtotal_to_display()));
								$total += $the_order->order_total;
							}
						}
					}
					echo wc_price( $total );

					if ( $gift_card->amount && ( $total > $gift_card->amount ) ) {
						$percent = (float) ( $total - $gift_card->amount ) / $gift_card->amount * 100;
						echo '<br><span class="smgc-percent">' . sprintf( __( '(+ %.2f%%)', 'smms-woocommerce-gift-cards' ), $percent ) . '</span>';
					}

					break;

				case SMGC_TABLE_COLUMN_ACTIONS:

					SMMS_SMGC()->admin->show_change_status_button( $post_ID, $gift_card );
					SMMS_SMGC()->admin->show_send_email_button( $post_ID, $gift_card );

					break;
			}
		}



		/**
		 * @param int                    $post_ID
		 * @param SMGC_Gift_Card_Premium $gift_card
		 */
		public function show_details_on_gift_cards_table( $post_ID, $gift_card ) {

			if ( $gift_card->is_dismissed() ) {
				?>
				<span
					class="smgc-dismissed-text"><?php _e( "This card is dismissed.", 'smms-woocommerce-gift-cards' ); ?></span>
				<?php
			}

			if ( ! $gift_card->is_digital ) {
				?>
				<div>
					<span><?php echo __( "Physical product", 'smms-woocommerce-gift-cards' ); ?></span>
				</div>
				<?php
			} else {

				if ( $gift_card->delivery_send_date ) {
					$status_class = "sent";
					$message      = sprintf( __( "Sent on %s", 'smms-woocommerce-gift-cards' ), $gift_card->delivery_send_date );
				} else if ( $gift_card->delivery_date >= current_time( 'Y-m-d' ) ) {
					$status_class = "scheduled";
					$message      = __( "Scheduled", 'smms-woocommerce-gift-cards' );
				} else {
					$status_class = "failed";
					$message      = __( "Failed", 'smms-woocommerce-gift-cards' );
				}
				?>

				<div>
					<span><?php echo sprintf( __( "Recipient: %s", 'smms-woocommerce-gift-cards' ), $gift_card->recipient ); ?></span>
				</div>
				<div>
					<span><?php echo sprintf( __( "Delivery date: %s", 'smms-woocommerce-gift-cards' ), $gift_card->delivery_date ); ?></span>
					<br>
					<span
						class="smgc-delivery-status <?php echo $status_class; ?>"><?php echo $message; ?></span>

				</div>
				<?php
			}
		}
	}
}

SMMS_SMGC_Gift_Cards_Table::get_instance();