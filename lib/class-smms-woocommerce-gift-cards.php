<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( ! class_exists( 'SMMS_WooCommerce_Gift_Cards' ) ) {

	/**
	 *
	 * @class   SMMS_WooCommerce_Gift_Cards
	 *
	 * @since   1.0.0
	 * @author Lorenzo Giuffrida
	 */
	class SMMS_WooCommerce_Gift_Cards {
		/**
		 * @var SMMS_SMGC_Backend|SMMS_SMMS_Backend_Premium The instance for backend features and methods
		 */
		public $admin;

		/**
		 * @var SMMS_SMMS_Frontend|SMMS_SMGC_Frontend_Premium instance for frontend features and methods
		 */
		public $frontend;

		/**
		 * Single instance of the class
		 *
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * Initialize plugin and registers actions and filters to be used
		 *
		 * @since  1.0
		 * @author Lorenzo Giuffrida
		 */
		protected function __construct() {

			$this->includes();
			$this->init_hooks();
		}

		public function prior_than_150() {
			return version_compare( SMMS_SMGC_VERSION, '1.5.0', '<' );
		}

		public function includes() {
			require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-product.php' );
			require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-cart-checkout.php' );
			require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-emails.php' );
			require_once( SMMS_SMGC_DIR . 'lib/class-smms-smgc-gift-cards-table.php' );

			/**
			 * Include third-party integration classes
			 */

			//  SMMS Dynamic Pricing
			defined( 'SMMS_YWDPD_VERSION' ) && require_once( SMMS_SMGC_DIR . 'lib/third-party/class-smgc-dynamic-pricing.php' );

			//  SMMS Points and Rewards
			defined( 'SMMS_YWPAR_VERSION' ) && require_once( SMMS_SMGC_DIR . 'lib/third-party/class-smgc-points-and-rewards.php' );

			//  SMMS Multi Vendor
			defined( 'SMMS_WPV_PREMIUM' ) && require_once( SMMS_SMGC_DIR . 'lib/third-party/class-smgc-multi-vendor-module.php' );

			//  Aelia Currency Switcher
			class_exists( 'WC_Aelia_CurrencySwitcher' ) && require_once( SMMS_SMGC_DIR . 'lib/third-party/class-smgc-AeliaCS-module.php' );

			defined( 'SMMS_WCQV_PREMIUM' ) && require_once( SMMS_SMGC_DIR . 'lib/third-party/class-smgc-general-integrations.php' );

			//  WPML
			global $woocommerce_wpml;
			$woocommerce_wpml && isset($woocommerce_wpml->multi_currency) && require_once( SMMS_SMGC_DIR . 'lib/third-party/class-smgc-wpml.php' );

		}

		public function init_hooks() {
			/**
			 * Do some stuff on plugin init
			 */
			add_action( 'init', array( $this, 'on_plugin_init' ) );

			/**
			 * Hide the temporary gift card product from being shown on shop page
			 */
			add_action( 'woocommerce_product_query', array( $this, 'hide_from_shop_page' ), 10, 1 );

			add_filter( 'smms_plugin_status_sections', array( $this, 'set_plugin_status' ) );
		}


		/**
		 * Hide the temporary gift card product from being shown on shop page
		 *
		 * @param WP_Query $query The current query
		 *
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function hide_from_shop_page( $query ) {
			$default_gift_card = get_option( SMGC_PRODUCT_PLACEHOLDER, - 1 );

			if ( $default_gift_card > 0 ) {
				$query->set( 'post__not_in', array( $default_gift_card ) );
			}
		}

		/**
		 * Execute update on data used by the plugin that has been changed passing
		 * from a DB version to another
		 */
		public function update_database() {

			/**
			 * Init DB version if not exists
			 */
			$db_version = get_option( SMGC_DB_VERSION_OPTION );

			if ( ! $db_version ) {
				//  Update from previous version where the DB option was not set
				global $wpdb;

				//  Update metakey from SMMS Gift Cards 1.0.0
				$query = "Update {$wpdb->prefix}woocommerce_order_itemmeta
                        set meta_key = '" . SMGC_META_GIFT_CARD_POST_ID . "'
                        where meta_key = 'gift_card_post_id'";
				$wpdb->query( $query );

				$db_version = '1.0.0';
			}

			/**
			 * Start the database update step by step
			 */
			if ( version_compare( $db_version, '1.0.0', '<=' ) ) {

				//  Set gift card placeholder with catalog visibility equal to "hidden"
				$placeholder_id = get_option( SMGC_PRODUCT_PLACEHOLDER );

				update_post_meta( $placeholder_id, '_visibility', 'hidden' );

				$db_version = '1.0.1';
			}

			if ( version_compare( $db_version, '1.0.1', '<=' ) ) {

				//  extract the user_id from the order where a gift card is applied and register
				//  it so the gift card will be shown on my-account

				$args = array(
					'numberposts' => - 1,
					'meta_key'    => SMGC_META_GIFT_CARD_ORDERS,
					'post_type'   => SMGC_CUSTOM_POST_TYPE_NAME,
					'post_status' => 'any',
				);

				//  Retrieve the gift cards matching the criteria
				$posts = get_posts( $args );

				foreach ( $posts as $post ) {
					$gift_card = new SMGC_Gift_Card_Premium( array( 'ID' => $post->ID ) );

					if ( ! $gift_card->exists() ) {
						continue;
					}

					/** @var WC_Order $order */
					$orders = $gift_card->get_registered_orders();
					foreach ( $orders as $order_id ) {
						$order = wc_get_order( $order_id );
						if ( $order ) {
							$gift_card->register_user( $order->customer_user );
						}
					}
				}

				$db_version = '1.0.2';  //  Continue to next step...
			}

			//  Update the current DB version
			update_option( SMGC_DB_VERSION_OPTION, SMMS_SMGC_DB_CURRENT_VERSION );
		}

		/**
		 *  Execute all the operation need when the plugin init
		 */
		public function on_plugin_init() {
			$this->init_post_type();

			$this->init_plugin();

			$this->update_database();
		}

		/**
		 * Initialize plugin data and shard instances
		 */
		public function init_plugin() {
			//nothing to do
		}

		/**
		 * Register the custom post type
		 */
		public function init_post_type() {
			$args = array(
				'label'               => __( 'Gift Cards', 'smms-woocommerce-gift-cards' ),
				'description'         => __( 'Gift Cards', 'smms-woocommerce-gift-cards' ),
				//'labels' => $labels,
				// Features this CPT supports in Post Editor
				'supports'            => array(
					//'title',
					'editor',
					//'author',
				),
				'hierarchical'        => false,
				'public'              => false,
				'show_ui'             => false,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => false,
				'show_in_admin_bar'   => false,
				'menu_position'       => 9,
				'can_export'          => false,
				'has_archive'         => false,
				'exclude_from_search' => true,
				'menu_icon'           => 'dashicons-clipboard',
				'query_var'           => false,
			);

			// Registering your Custom Post Type
			register_post_type( SMGC_CUSTOM_POST_TYPE_NAME, $args );
		}

		/**
		 * Checks for SMMS_SMGC_Gift_Card instance
		 *
		 * @param object $obj the object to check
		 *
		 * @return bool obj is an instance of SMMS_SMGC_Gift_Card
		 */
		public function instanceof_giftcard( $obj ) {
			return $obj instanceof SMMS_SMGC_Gift_Card;
		}

		/**
		 * Retrieve a gift card product instance from the gift card code
		 *
		 * @param $code string the card code to search for
		 *
		 * @return SMMS_SMGC_Gift_Card
		 */
		public function get_gift_card_by_code( $code ) {
			/*if ( ! is_string ( $code ) ) {
				return null;
			}
*/
			$args = array( 'gift_card_number' => $code );

			return new SMMS_SMGC_Gift_Card( $args );
		}

		/**
		 * Generate a new gift card code
		 *
		 *
		 * @return bool
		 * @author Lorenzo Giuffrida
		 * @since  1.0.0
		 */
		public function generate_gift_card_code() {

			//  Create a new gift card number

			//http://stackoverflow.com/questions/3521621/php-code-for-generating-decent-looking-coupon-codes-mix-of-alphabets-and-number
			$code = strtoupper( substr( base_convert( sha1( uniqid( mt_rand() ) ), 16, 36 ), 0, 16 ) );

			$code = sprintf( "%s-%s-%s-%s",
				substr( $code, 0, 4 ),
				substr( $code, 4, 4 ),
				substr( $code, 8, 4 ),
				substr( $code, 12, 4 )
			);

			return $code;
		}
	}
}

