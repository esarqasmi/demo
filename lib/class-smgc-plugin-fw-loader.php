<?php

if ( ! defined ( 'ABSPATH' ) ) {

    exit; // Exit if accessed directly

}



if ( ! class_exists ( 'SMGC_Plugin_FW_Loader' ) ) {



    /**

     *

     * @class   SMGC_Plugin_FW_Loader

     *

     * @since   1.0.0

     * @author Lorenzo Giuffrida

     */

    class SMGC_Plugin_FW_Loader {



        /**

         * @var $_panel Panel Object

         */

        protected $_panel;



        /**

         * @var $_premium string Premium tab template file name

         */

        protected $_premium = 'premium.php';



        /**

         * @var string the SMMS plugin stats page

         */

        protected $_status_page = 'status.php';



        /**

         * @var string Premium version landing link

         */

        protected $_premium_landing = '//smmsemes.com/themes/plugins/smms-woocommerce-gift-cards/';



        /**

         * @var string Plugin official documentation

         */

        protected $_official_documentation = '//smmsemes.com/docs-plugins/smms-woocommerce-gift-cards/';



        /**

         * @var string Plugin panel page

         */

        protected $_panel_page = 'smms_woocommerce_gift_cards_panel';



        /**

         * Single instance of the class

         *

         * @since 1.0.0

         */

        protected static $instance;



        /**

         * Returns single instance of the class

         *

         * @since 1.0.0

         */

        public static function get_instance () {

            if ( is_null ( self::$instance ) ) {

                self::$instance = new self();

            }



            return self::$instance;

        }



        public function __construct () {



            $this->plugin_fw_loader ();

	        /**

             * Register actions and filters to be used for creating an entry on YIT Plugin menu

             */

            add_action ( 'admin_init', array ( $this, 'register_pointer' ) );



            //Add action links

            add_filter ( 'plugin_action_links_' . plugin_basename ( SMMS_SMGC_DIR . '/' . basename ( SMMS_SMGC_FILE ) ), array (

                $this,

                'action_links',

            ) );



            add_filter ( 'plugin_row_meta', array ( $this, 'plugin_row_meta' ), 10, 4 );



            //  Add stylesheets and scripts files

            add_action ( 'admin_menu', array ( $this, 'register_panel' ), 5 );



            //  Show plugin premium tab

            add_action ( 'smms_gift_cards_tab_premium', array ( $this, 'premium_tab' ) );

            /**

             * register plugin to licence/update system

             */

            $this->licence_activation ();



        }



        /**

         * Load YIT core plugin

         *

         * @since  1.0

         * @access public

         * @return void

         * @author Andrea Grillo <andrea.grillo@smmsemes.com>

         */

        public function plugin_fw_loader () {

            if ( ! defined ( 'YIT_CORE_PLUGIN' ) ) {

                global $plugin_fw_data;

                if ( ! empty( $plugin_fw_data ) ) {

                    $plugin_fw_file = array_shift ( $plugin_fw_data );

                    require_once ( $plugin_fw_file );

                }

            }

        }



        /**

         * Add a panel under SMMS Plugins tab

         *

         * @return   void

         * @since    1.0

         * @author   Andrea Grillo <andrea.grillo@smmsemes.com>

         * @use      /Yit_Plugin_Panel class

         * @see      plugin-fw/lib/yit-plugin-panel.php

         */

        public function register_panel () {



            if ( ! empty( $this->_panel ) ) {

                return;

            }



            $admin_tabs[ 'general' ]  = __ ( 'General', 'smms-woocommerce-gift-cards' );

            $admin_tabs[ 'template' ] = __ ( 'Template', 'smms-woocommerce-gift-cards' );



            if ( ! defined ( 'SMMS_SMGC_PREMIUM' ) ) {

                $admin_tabs[ 'premium-landing' ] = __ ( 'Premium Version', 'smms-woocommerce-gift-cards' );

            }

            $args = array (

                'create_menu_page' => true,

                'parent_slug'      => '',

                'page_title'       => 'Gift Cards',

                'menu_title'       => 'Gift Cards',

                'capability'       => 'manage_options',

                'parent'           => '',

                'parent_page'      => 'yit_plugin_panel',

                'page'             => $this->_panel_page,

                'admin-tabs'       => $admin_tabs,

                'options-path'     => SMMS_SMGC_DIR . 'plugin-options',

            );



            /* === Fixed: not updated theme  === */

            if ( ! class_exists ( 'YIT_Plugin_Panel_WooCommerce' ) ) {



                require_once ( SMMS_SMGC_DIR . 'plugin-fw/lib/yit-plugin-panel-wc.php' );

            }



            $this->_panel = new YIT_Plugin_Panel_WooCommerce( $args );



            if ( defined ( 'SMMS_SMGC_PREMIUM' ) ) {

                add_action ( 'woocommerce_admin_field_smgc_upload_image', array ( $this->_panel, 'yit_upload' ), 10, 1 );

            }

        }



        /**

         * Premium Tab Template

         *

         * Load the premium tab template on admin page

         *

         * @return   void

         * @since    1.0

         * @author   Andrea Grillo <andrea.grillo@smmsemes.com>

         * @return void

         */

        public function premium_tab () {

            $premium_tab_template = SMMS_SMGC_TEMPLATES_DIR . 'admin/' . $this->_premium;

            if ( file_exists ( $premium_tab_template ) ) {

                include_once ( $premium_tab_template );

            }

        }



        /**

         * Action Links

         *

         * add the action links to plugin admin page

         *

         * @param $links | links plugin array

         *

         * @return   mixed Array

         * @since    1.0

         * @author   Andrea Grillo <andrea.grillo@smmsemes.com>

         * @return mixed

         * @use      plugin_action_links_{$plugin_file_name}

         */

        public function action_links ( $links ) {

            $links[] = '<a href="' . admin_url ( "admin.php?page={$this->_panel_page}" ) . '">' . __ ( 'Settings', 'smms-woocommerce-gift-cards' ) . '</a>';





            if ( defined ( 'SMMS_SMGC_FREE_INIT' ) ) {

                $links[] = '<a href="' . $this->get_premium_landing_uri () . '" target="_blank">' . __ ( 'Premium Version', 'smms-woocommerce-gift-cards' ) . '</a>';

            }



            return $links;

        }



        /**

         * plugin_row_meta

         *

         * add the action links to plugin admin page

         *

         * @param $plugin_meta

         * @param $plugin_file

         * @param $plugin_data

         * @param $status

         *

         * @return   Array

         * @since    1.0

         * @author   Andrea Grillo <andrea.grillo@smmsemes.com>

         * @use      plugin_row_meta

         */

        public function plugin_row_meta ( $plugin_meta, $plugin_file, $plugin_data, $status ) {



            if ( ( defined ( 'SMMS_SMGC_INIT' ) && ( SMMS_SMGC_INIT == $plugin_file ) ) ||

                ( defined ( 'SMMS_SMGC_FREE_INIT' ) && ( SMMS_SMGC_FREE_INIT == $plugin_file ) )

            ) {

                //$plugin_meta[] = '<a href="' . $this->_official_documentation . '" target="_blank">' . __ ( 'Plugin Documentation', 'smms-woocommerce-gift-cards' ) . '</a>';

            }



            return $plugin_meta;

        }



        public function register_pointer () {

            if ( ! class_exists ( 'YIT_Pointers' ) ) {

                include_once ( 'plugin-fw/lib/yit-pointers.php' );

            }



            $premium_message = defined ( 'SMMS_SMGC_PREMIUM' )

                ? ''

                : __ ( 'SMMS WooCommerce Gift Cards is available in an outstanding PREMIUM version with many new options, discover it now.', 'smms-woocommerce-gift-cards' ) .

                ' <a href="' . $this->get_premium_landing_uri () . '">' . __ ( 'Premium version', 'smms-woocommerce-gift-cards' ) . '</a>';



            $args[] = array (

                'screen_id'  => 'plugins',

                'pointer_id' => 'smms_woocommerce_gift_cards',

                'target'     => '#toplevel_page_yit_plugin_panel',

                'content'    => sprintf ( '<h3> %s </h3> <p> %s </p>',

                    __ ( 'SMMS WooCommerce Gift Cards', 'smms-woocommerce-gift-cards' ),

                    __ ( 'In the SMMS Plugins tab you can find SMMS WooCommerce Gift Cards options.<br> From this menu you can access all the settings of your active SMMS plugins.', 'smms-woocommerce-gift-cards' ) . '<br>' . $premium_message

                ),

                'position'   => array ( 'edge' => 'left', 'align' => 'center' ),

                'init'       => defined ( 'SMMS_SMGC_PREMIUM' ) ? SMMS_SMGC_INIT : SMMS_SMGC_FREE_INIT,

            );



            YIT_Pointers ()->register ( $args );

        }



        /**

         * Get the premium landing uri

         *

         * @since   1.0.0

         * @author  Andrea Grillo <andrea.grillo@smmsemes.com>

         * @return  string The premium landing link

         */

        public function get_premium_landing_uri () {

            return defined ( 'SMMS_REFER_ID' ) ? $this->_premium_landing . '?refer_id=' . SMMS_REFER_ID : $this->_premium_landing . '?refer_id=1030585';

        }



        //region    ****    licence related methods ****



        /**

         * Add actions to manage licence activation and updates

         */

        public function licence_activation () {

            if ( ! defined ( 'SMMS_SMGC_PREMIUM' ) ) {

                return;

            }



            add_action ( 'wp_loaded', array ( $this, 'register_plugin_for_activation' ), 99 );

            add_action ( 'admin_init', array ( $this, 'register_plugin_for_updates' ) );

        }



        /**

         * Register plugins for activation tab

         *

         * @return void

         * @since    2.0.0

         * @author   Andrea Grillo <andrea.grillo@smmsemes.com>

         */

        public function register_plugin_for_activation () {



            if ( ! class_exists ( 'YIT_Plugin_Licence' ) ) {

                require_once 'plugin-fw/lib/yit-plugin-licence.php';

            }



            YIT_Plugin_Licence ()->register ( SMMS_SMGC_INIT, SMMS_SMGC_SECRET_KEY, SMMS_SMGC_SLUG );

        }



        /**

         * Register plugins for update tab

         *

         * @return void

         * @since    2.0.0

         * @author   Andrea Grillo <andrea.grillo@smmsemes.com>

         */

        public function register_plugin_for_updates () {

            if ( ! class_exists ( 'YIT_Upgrade' ) ) {

                require_once 'plugin-fw/lib/yit-upgrade.php';

            }

            YIT_Upgrade ()->register ( SMMS_SMGC_SLUG, SMMS_SMGC_INIT );

        }

        //endregion

    }

}

