<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'SMGC_General_Integrations' ) ) {

	/**
	 *
	 * @class   SMGC_General_Integrations
	 *
	 * @since   1.0.0
	 * @author Lorenzo Giuffrida
	 */
	class SMGC_General_Integrations {

		/**
		 * Single instance of the class
		 *
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @since 1.0.0
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public function __construct() {
			/**
			 * SMMS Quick View. Let the SMMS Gift Cards be enqueued in page other than the single product page
			 */
			add_filter( 'smms_smgc_do_eneuque_frontend_scripts', array(
				$this,
				'enqueue_script_wide'
			) );
		}

		public function enqueue_script_wide() {
			return defined( 'SMMS_WCQV_PREMIUM' ) && ( is_shop() || is_product_category() || is_product_tag() );
		}
	}
}

SMGC_General_Integrations::get_instance();